

----------------------------------------------------------------------------------------------------------------------------------------------------------------------
/* Name : FP Customer 360    */
/* Schedule : Runs 1st day of every month*/
/* Description : Creates temporal features of consumer behavior in Fairprice */
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
--modify year and date
drop table if exists #date;
create table #date as
(select 2020 as year_today, 
202008 as year_month_today);
select * from #date;

-- Merge sales data and store data
drop table if exists #fp_temp;
create table #fp_temp
distkey(invoice_no)
sortkey(invoice_no)
as
(select x.master_id,y.link_id as csn, z.*
from fp.fp_sale_bridge_master as x
left join fp.fp_customer_master as y on y.master_id = x.master_id
left join 
(select 
business_date, A.store_code,invoice_date,till_code,invoice_no,proof_of_identity,loyalty_card_no,sale_tot_qty,sale_net_val,loyalty_points_issued,bonus_points_redeemed,
extract(year from business_date) as yearid , 
extract(month from business_date) as monthid, 
((yearid*100)+(monthid*1)) as year_month,
extract(day from business_date) as dayid,
((yearid*10000)+(monthid*100)+(dayid)) as year_month_day,
datepart(week,business_date) as week_no ,
store_description,
business_unit,
store_group,store_format,
zip
from fp.fp_sale_head A left join fp.fp_store B on A.store_code=B.store_code) z
on z.business_date = x.business_date and z.till_code = x.till_code and z.invoice_no = x.invoice_no and z.store_code = x.store_code);

-- select * from fp.fp_sale_head where proof_of_identity = '00078628406db29fe6b91321a3fa4fb1a7ad2669';
-- select * from #fp_temp where proof_of_identity = '00078628406db29fe6b91321a3fa4fb1a7ad2669';
select top 10 * from #fp_temp;  
select count( distinct proof_of_identity) from #fp_temp;
--1,560,687
--190,464,872

--checks
select year_month,store_group, count(distinct proof_of_identity) as count_cust ,sum(sale_net_val) as total_gmv
from #fp_temp group by year_month,store_group;
select distinct store_description,business_unit,store_group,store_format from #fp_temp;
select distinct store_description,business_unit,store_group,store_format from fp.fp_store;

-- check missing CSN
select count(*) from #fp_temp where csn is null;
select top 10 * from #fp_temp where csn is null;
select distinct(loyalty_card_no) from #fp_temp where csn is null;
select max(business_date) from #fp_temp where csn is null;

select * from link.link_cls_card where func_sha1(card_no)='3b8979385b0d8b5a2086000723128dbec16580b8';



-------------------------------------------------------------------------------------------------------------------------------------------------------------------
/* Next month feature creation */
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- keep aside data for creating next month features
drop table if exists #dat_trx_nextm;
create temp table #dat_trx_nextm 
distkey(invoice_no)
sortkey(invoice_no)
as 
(select *,(business_date::varchar(20) || store_code || till_code || invoice_no) as trx_code
from #fp_temp  
where sale_net_val>0 and store_group in ('FAIRPRICE','FINEST','HYPERMARKET','ON-LINE')
and year_month = (select year_month_today from #date) and csn is not null);

select top 10 * from #dat_trx_nextm;
select count(*) from #dat_trx_nextm;
select count( distinct proof_of_identity) from #dat_trx_nextm;
select distinct(loyalty_card_no) from #dat_trx_nextm where csn is null;

-- Summarize sales data by POI,trx code
drop table if exists #dat_trx_sumry_nextm;
create temp table #dat_trx_sumry_nextm 
distkey(csn)
sortkey(csn)
as
(select 
csn, 
store_code,
trx_code, 
store_description, 
store_group,
business_unit,
store_format,
zip as store_zip ,
business_date,
yearid,
monthid,
year_month,
week_no,
sum(sale_net_val) as total_spend,
sum(sale_tot_qty) as total_qty,
count(*) as total_cnt,
sum(loyalty_points_issued) as total_points_issued,
sum(bonus_points_redeemed) as total_points_redeemed,
count(distinct store_code) as total_uniq_stores
from #dat_trx_nextm
where sale_net_val>0
group by csn,trx_code,store_code,store_description, store_group,business_unit,store_format,
store_zip,business_date,yearid,monthid,year_month,week_no);

select top 10 * from #dat_trx_sumry_nextm;
select count(*) from #dat_trx_sumry_nextm;
select count(distinct csn) from #dat_trx_sumry_nextm;
select * from #dat_trx_sumry_nextm where total_cnt>1;

drop table if exists #dat_spend_nextm;
create table #dat_spend_nextm as
(select 
csn,
year_month as next_month,
max(business_date) as last_trx_date_nextm,
min(business_date) as first_trx_date_nextm,
sum(total_spend) as total_spend_nextm,
count(distinct trx_code) as total_trx_nextm,
sum(total_points_issued) as total_points_issued_nextm,
sum(total_points_redeemed) as total_points_redeemed_nextm
from #dat_trx_sumry_nextm 
group by csn,year_month);
select top 10 * from #dat_spend_nextm;
select count(*) from #dat_spend_nextm;
select count(distinct csn) from #dat_spend_nextm;

-------------------------------------------------------------------------------------------------------------------------------------------------------------------
/* Historical month feature creation */
-------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- Filter out store formats relevant to Core customers (these are the only store formats available :)) 
drop table if exists #dat_trx;
create temp table #dat_trx 
distkey(invoice_no)
sortkey(invoice_no)
as 
(select *,(business_date::varchar(20) || store_code || till_code || invoice_no) as trx_code
from #fp_temp  
where sale_net_val>0 and store_group in ('FAIRPRICE','FINEST','HYPERMARKET','ON-LINE')
and year_month < (select year_month_today from #date) and csn is not null);

select top 10 * from #dat_trx;
select count(distinct csn) from #dat_trx;
select count( distinct proof_of_identity) from #dat_trx;
select distinct(loyalty_card_no) from #dat_trx where csn is null;

-- checks
select invoice_date::date, count(distinct proof_of_identity) as cnt from #dat_trx
where proof_of_identity in (select distinct proof_of_identity from #dat_trx where yearid=2018) and monthid='06' and yearid=2019
group by invoice_date::date;


-- Summarize sales data by POI,trx code
drop table if exists #dat_trx_sumry;
create temp table #dat_trx_sumry 
distkey(csn)
sortkey(csn)
as
(select csn, store_code,trx_code, store_description, 
store_group,business_unit,store_format,zip as store_zip ,
business_date,yearid,monthid,year_month,week_no,
sum(sale_net_val) as total_spend,
sum(sale_tot_qty) as total_qty,
count(*) as total_cnt,
sum(loyalty_points_issued) as total_points_issued,
sum(bonus_points_redeemed) as total_points_redeemed,
count(distinct store_code) as total_uniq_stores
from #dat_trx
where sale_net_val>0
group by csn,trx_code,store_code,store_description, store_group,business_unit,store_format,
store_zip,business_date,yearid,monthid,year_month,week_no);

select top 10 * from #dat_trx_sumry;
select count(*) from #dat_trx_sumry;
select count(distinct csn) from #dat_trx_sumry;
select * from #dat_trx_sumry where total_cnt>1;
--1552334

-- check for duplicate records
select * from #dat_trx where trx_code in (select trx_code from #dat_trx_sumry where total_cnt>1);


------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Create spend, trx, LP issued, redeemed features
------------------------------------------------------------------------------------------------------------------------------------------------------------------

------------------------------------- Overall -------------------------------------------------------
drop table if exists #dat_spend_all;
create table #dat_spend_all as
(select 
csn,
sum(total_spend) as total_spend,
count(distinct trx_code) as total_trx,
sum(total_points_issued) as total_points_issued,
sum(total_points_redeemed) as total_points_redeemed
from #dat_trx_sumry 
group by csn);
select top 10 * from #dat_spend_all;
select count(*) from #dat_spend_all;
select count(distinct csn) from #dat_spend_all;

------------------------------------- Yearwise -------------------------------------------------------
drop table if exists #temp;
create temp table #temp 
distkey(csn)
sortkey(csn)
as
(select csn, business_date,total_spend,trx_code,
datepart('years',business_date) as trx_year,
case when datepart('years',business_date)=2015 then total_spend else 0 end as total_spend_2015,
case when datepart('years',business_date)=2016 then total_spend else 0 end as total_spend_2016,
case when datepart('years',business_date)=2017 then total_spend else 0 end as total_spend_2017,
case when datepart('years',business_date)=2018 then total_spend else 0 end as total_spend_2018,
case when datepart('years',business_date)=2019 then total_spend else 0 end as total_spend_2019,
case when datepart('years',business_date)=2020 then total_spend else 0 end as total_spend_2020,

case when datepart('years',business_date)=2015 then trx_code else NULL end as total_trx_2015,
case when datepart('years',business_date)=2016 then trx_code else NULL end as total_trx_2016,
case when datepart('years',business_date)=2017 then trx_code else NULL end as total_trx_2017,
case when datepart('years',business_date)=2018 then trx_code else NULL end as total_trx_2018,
case when datepart('years',business_date)=2019 then trx_code else NULL end as total_trx_2019,
case when datepart('years',business_date)=2020 then trx_code else NULL end as total_trx_2020,

case when datepart('years',business_date)=2015 then total_points_issued else NULL end as total_points_issued_2015,
case when datepart('years',business_date)=2016 then total_points_issued else NULL end as total_points_issued_2016,
case when datepart('years',business_date)=2017 then total_points_issued else NULL end as total_points_issued_2017,
case when datepart('years',business_date)=2018 then total_points_issued else NULL end as total_points_issued_2018,
case when datepart('years',business_date)=2019 then total_points_issued else NULL end as total_points_issued_2019,
case when datepart('years',business_date)=2020 then total_points_issued else NULL end as total_points_issued_2020,

case when datepart('years',business_date)=2015 then total_points_redeemed else NULL end as total_points_redeemed_2015,
case when datepart('years',business_date)=2016 then total_points_redeemed else NULL end as total_points_redeemed_2016,
case when datepart('years',business_date)=2017 then total_points_redeemed else NULL end as total_points_redeemed_2017,
case when datepart('years',business_date)=2018 then total_points_redeemed else NULL end as total_points_redeemed_2018,
case when datepart('years',business_date)=2019 then total_points_redeemed else NULL end as total_points_redeemed_2019,
case when datepart('years',business_date)=2020 then total_points_redeemed else NULL end as total_points_redeemed_2020
from #dat_trx_sumry);

--check
select top 10 * from #temp;
select * from #temp where csn='000088609cc5aac77909b8d1a7aaeea14a363d41';

drop table if exists #dat_spend_yrwise;
create temp table #dat_spend_yrwise 
distkey(csn)
sortkey(csn)
as
(select csn,
sum(total_spend_2015) as total_spend_2015,
sum(total_spend_2016) as total_spend_2016,
sum(total_spend_2017) as total_spend_2017,
sum(total_spend_2018) as total_spend_2018,
sum(total_spend_2019) as total_spend_2019,
sum(total_spend_2020) as total_spend_2020,
count(distinct total_trx_2015) as total_trx_2015,
count(distinct total_trx_2016) as total_trx_2016,
count(distinct total_trx_2017) as total_trx_2017,
count(distinct total_trx_2018) as total_trx_2018,
count(distinct total_trx_2019) as total_trx_2019,
count(distinct total_trx_2020) as total_trx_2020,
sum(total_points_issued_2015) as total_points_issued_2015,
sum(total_points_issued_2016) as total_points_issued_2016,
sum(total_points_issued_2017) as total_points_issued_2017,
sum(total_points_issued_2018) as total_points_issued_2018,
sum(total_points_issued_2019) as total_points_issued_2019,
sum(total_points_issued_2020) as total_points_issued_2020,
sum(total_points_redeemed_2015) as total_points_redeemed_2015,
sum(total_points_redeemed_2016) as total_points_redeemed_2016,
sum(total_points_redeemed_2017) as total_points_redeemed_2017,
sum(total_points_redeemed_2018) as total_points_redeemed_2018,
sum(total_points_redeemed_2019) as total_points_redeemed_2019,
sum(total_points_redeemed_2020) as total_points_redeemed_2020
from #temp group by csn);

--check
select top 10 * from #dat_spend_yrwise;
select * from #dat_spend_yrwise where csn='000088609cc5aac77909b8d1a7aaeea14a363d41';


select top 10 * from #temp;
select * from #temp where csn='000088609cc5aac77909b8d1a7aaeea14a363d41';

----- monthly spend 2019
drop table if exists #dat_spend_mnthwise;
create temp table #dat_spend_mnthwise 
distkey(csn)
sortkey(csn)
as
(select csn,
sum(case when ((datepart('years',business_date)*100)+datepart('months',business_date))= 201801 then total_spend else 0 end) as total_spend_201801,
sum(case when ((datepart('years',business_date)*100)+datepart('months',business_date))= 201802 then total_spend else 0 end) as total_spend_201802,
sum(case when ((datepart('years',business_date)*100)+datepart('months',business_date))= 201803 then total_spend else 0 end) as total_spend_201803,
sum(case when ((datepart('years',business_date)*100)+datepart('months',business_date))= 201804 then total_spend else 0 end) as total_spend_201804,
sum(case when ((datepart('years',business_date)*100)+datepart('months',business_date))= 201805 then total_spend else 0 end) as total_spend_201805,
sum(case when ((datepart('years',business_date)*100)+datepart('months',business_date))= 201806 then total_spend else 0 end) as total_spend_201806,
sum(case when ((datepart('years',business_date)*100)+datepart('months',business_date))= 201807 then total_spend else 0 end) as total_spend_201807,
sum(case when ((datepart('years',business_date)*100)+datepart('months',business_date))= 201808 then total_spend else 0 end) as total_spend_201808,
sum(case when ((datepart('years',business_date)*100)+datepart('months',business_date))= 201809 then total_spend else 0 end) as total_spend_201809,
sum(case when ((datepart('years',business_date)*100)+datepart('months',business_date))= 201810 then total_spend else 0 end) as total_spend_201810,
sum(case when ((datepart('years',business_date)*100)+datepart('months',business_date))= 201811 then total_spend else 0 end) as total_spend_201811,
sum(case when ((datepart('years',business_date)*100)+datepart('months',business_date))= 201812 then total_spend else 0 end) as total_spend_201812,
sum(case when ((datepart('years',business_date)*100)+datepart('months',business_date))= 201901 then total_spend else 0 end) as total_spend_201901,
sum(case when ((datepart('years',business_date)*100)+datepart('months',business_date))= 201902 then total_spend else 0 end) as total_spend_201902,
sum(case when ((datepart('years',business_date)*100)+datepart('months',business_date))= 201903 then total_spend else 0 end) as total_spend_201903,
sum(case when ((datepart('years',business_date)*100)+datepart('months',business_date))= 201904 then total_spend else 0 end) as total_spend_201904,
sum(case when ((datepart('years',business_date)*100)+datepart('months',business_date))= 201905 then total_spend else 0 end) as total_spend_201905,
sum(case when ((datepart('years',business_date)*100)+datepart('months',business_date))= 201906 then total_spend else 0 end) as total_spend_201906,
sum(case when ((datepart('years',business_date)*100)+datepart('months',business_date))= 201907 then total_spend else 0 end) as total_spend_201907,
sum(case when ((datepart('years',business_date)*100)+datepart('months',business_date))= 201908 then total_spend else 0 end) as total_spend_201908,
sum(case when ((datepart('years',business_date)*100)+datepart('months',business_date))= 201909 then total_spend else 0 end) as total_spend_201909,
sum(case when ((datepart('years',business_date)*100)+datepart('months',business_date))= 201910 then total_spend else 0 end) as total_spend_201910,
sum(case when ((datepart('years',business_date)*100)+datepart('months',business_date))= 201911 then total_spend else 0 end) as total_spend_201911,
sum(case when ((datepart('years',business_date)*100)+datepart('months',business_date))= 201912 then total_spend else 0 end) as total_spend_201912,
sum(case when ((datepart('years',business_date)*100)+datepart('months',business_date))= 202001 then total_spend else 0 end) as total_spend_202001,
sum(case when ((datepart('years',business_date)*100)+datepart('months',business_date))= 202002 then total_spend else 0 end) as total_spend_202002,
sum(case when ((datepart('years',business_date)*100)+datepart('months',business_date))= 202003 then total_spend else 0 end) as total_spend_202003,
sum(case when ((datepart('years',business_date)*100)+datepart('months',business_date))= 202004 then total_spend else 0 end) as total_spend_202004,
sum(case when ((datepart('years',business_date)*100)+datepart('months',business_date))= 202005 then total_spend else 0 end) as total_spend_202005,
sum(case when ((datepart('years',business_date)*100)+datepart('months',business_date))= 202006 then total_spend else 0 end) as total_spend_202006,
sum(case when ((datepart('years',business_date)*100)+datepart('months',business_date))= 202007 then total_spend else 0 end) as total_spend_202007,
sum(case when ((datepart('years',business_date)*100)+datepart('months',business_date))= 202008 then total_spend else 0 end) as total_spend_202008,
sum(case when ((datepart('years',business_date)*100)+datepart('months',business_date))= 202009 then total_spend else 0 end) as total_spend_202009,
sum(case when ((datepart('years',business_date)*100)+datepart('months',business_date))= 202010 then total_spend else 0 end) as total_spend_202010,
sum(case when ((datepart('years',business_date)*100)+datepart('months',business_date))= 202011 then total_spend else 0 end) as total_spend_202011,
sum(case when ((datepart('years',business_date)*100)+datepart('months',business_date))= 202012 then total_spend else 0 end) as total_spend_202012
from #temp group by csn);

--check
select top 10 * from #dat_spend_mnthwise;
select * from #dat_spend_mnthwise where csn='000088609cc5aac77909b8d1a7aaeea14a363d41';

------------------------------------- Last 12M  -------------------------------------------------------

drop table if exists #dat_spend_l12m;
create table #dat_spend_l12m as 
(select csn,
count(distinct year_month) as count_month_l12m,
sum(total_spend) as spend_l12m, 
count(distinct trx_code) as trx_l12m,
sum(total_points_issued) as total_points_issued_l12m,
sum(total_points_redeemed) as total_points_redeemed_l12m
from 
(select 
csn,
year_month,
trx_code,
total_spend,
total_points_issued,
total_points_redeemed
from #dat_trx_sumry 
where datediff('months',business_date,(select max(business_date) from #dat_trx))<12)
group by csn);
select top 10 * from #dat_spend_l12m;
select count(*) from #dat_spend_l12m;
select count( distinct csn) from #dat_spend_l12m;


------------------------------------- Last 6M  -------------------------------------------------------

drop table if exists #dat_spend_l6m;
create table #dat_spend_l6m as 
(select csn,
count(distinct year_month) as count_month_l6m,
sum(total_spend) as spend_l6m, 
count(distinct trx_code) as trx_l6m,
sum(total_points_issued) as total_points_issued_l6m,
sum(total_points_redeemed) as total_points_redeemed_l6m
from 
(select 
csn,
year_month,
trx_code,
total_spend,
total_points_issued,
total_points_redeemed
from #dat_trx_sumry 
where datediff('months',business_date,(select max(business_date) from #dat_trx)) <6)
group by csn);
select top 10 * from #dat_spend_l6m;
select count(*) from #dat_spend_l6m;
select count( distinct csn) from #dat_spend_l6m;

------------------------------------- Last 1M  -------------------------------------------------------

drop table if exists #dat_spend_l1m;
create table #dat_spend_l1m as 
(select csn,
count(distinct year_month) as count_month_l1m,
sum(total_spend) as spend_l1m, 
count(distinct trx_code) as trx_l1m,
sum(total_points_issued) as total_points_issued_l1m,
sum(total_points_redeemed) as total_points_redeemed_l1m
from 
(select 
csn,
year_month,
trx_code,
total_spend,
total_points_issued,
total_points_redeemed
from #dat_trx_sumry 
where datediff('months',business_date,(select max(business_date) from #dat_trx)) <1)
group by csn);
select top 10 * from #dat_spend_l1m;
select count(*) from #dat_spend_l1m;
select count( distinct csn) from #dat_spend_l1m;

------------------------------------- Last 3M  -------------------------------------------------------

drop table if exists #dat_spend_l3m;
create table #dat_spend_l3m as 
(select csn,
count(distinct year_month) as count_month_l3m,
sum(total_spend) as spend_l3m, 
count(distinct trx_code) as trx_l3m,
sum(total_points_issued) as total_points_issued_l3m,
sum(total_points_redeemed) as total_points_redeemed_l3m
from 
(select 
csn,
year_month,
trx_code,
total_spend,
total_points_issued,
total_points_redeemed
from #dat_trx_sumry 
where datediff('months',business_date,(select max(business_date) from #dat_trx)) <3)
group by csn);
select top 10 * from #dat_spend_l1m;
select count(*) from #dat_spend_l1m;
select count( distinct csn) from #dat_spend_l1m;

------------------------------------- First 6M  -------------------------------------------------------

-- create acquisition date features

drop table if exists #trx;
create temp table #trx 
distkey(csn)
sortkey(csn)
as (select *, row_number() over (partition by csn order by business_date) 
as row_num from #dat_trx_sumry);

select top 10 * from #trx;
select count(*) from #trx;
select count( distinct csn) from #trx;

drop table if exists #dat_acq;
create temp table #dat_acq
distkey(csn)
sortkey(csn)
as
(select 
csn,
business_date as date_of_acquisition,
(extract(year from date_of_acquisition)*100 +extract(month from date_of_acquisition)) as yearmonth_of_acquisition,
store_code as store_code_of_acquisition,
store_description as store_description_of_acquisition, 
store_group as store_group_of_acquisition,
business_unit as business_unit_of_acquisition,
datediff('days',date_of_acquisition,getdate()) as days_since_acquisition,
datediff('months',date_of_acquisition,getdate()) as months_since_acquisition,
datediff('years',date_of_acquisition,getdate()) as years_since_acquisition
from #trx where row_num=1 );

select top 10 * from #dat_acq;
select count(*) from #dat_acq;
select count( distinct csn) from #dat_acq;

---------------------------------------------------------------------------------------------------------------------------------------

drop table if exists #dat_spend_f6m;
create table #dat_spend_f6m as
(select 
csn,
sum(total_spend) as spend_f6m, 
count(distinct trx_code) as trx_f6m,
sum(total_points_issued) as total_points_issued_f6m,
sum(total_points_redeemed) as total_points_redeemed_f6m,
sum(total_points_issued)/sum(total_spend) as lp_per_dlr_f6m
from 
(select 
A.csn,
A.business_date,
A.trx_code,
A.total_spend,
A.total_points_issued,
A.total_points_redeemed,
B.yearmonth_of_acquisition,B.date_of_acquisition
from #dat_trx_sumry A left join #dat_acq B 
on A.csn=B.csn)
where datediff('months',date_of_acquisition,business_date) <=6
group by csn);

select top 10 * from #dat_spend_f6m;
select count(*) from #dat_spend_f6m;
select count(distinct csn) from #dat_spend_f6m;

------------------------------------- First 1M  -------------------------------------------------------

drop table if exists #dat_spend_f1m;
create table #dat_spend_f1m as
(select 
csn,
sum(total_spend) as spend_f1m, 
count(distinct trx_code) as trx_f1m,
sum(total_points_issued) as total_points_issued_f1m,
sum(total_points_redeemed) as total_points_redeemed_f1m,
sum(total_points_issued)/sum(total_spend) as lp_per_dlr_f1m
from 
(select 
A.csn,
A.business_date,
A.trx_code,
A.total_spend,
A.total_points_issued,
A.total_points_redeemed,
B.yearmonth_of_acquisition,B.date_of_acquisition
from #dat_trx_sumry A left join #dat_acq B 
on A.csn=B.csn)
where datediff('months',date_of_acquisition,business_date) <=1
group by csn);

select top 10 * from #dat_spend_f1m;
select count(*) from #dat_spend_f1m;
select count(distinct csn) from #dat_spend_f1m;

------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Merge in all spend features
------------------------------------------------------------------------------------------------------------------------------------------------------------------

drop table if exists #spend_tab;
create table #spend_tab
distkey(csn)
sortkey(csn)
as
(select 
A.csn,
next_month,
last_trx_date_nextm,
first_trx_date_nextm,
total_spend_nextm,
total_trx_nextm,
total_points_issued_nextm,
total_points_redeemed_nextm,
total_spend,
total_trx,
total_points_issued,
total_points_redeemed,
total_spend_2015,
total_spend_2016,
total_spend_2017,
total_spend_2018,
total_spend_2019,
total_spend_2020,
total_trx_2015,
total_trx_2016,
total_trx_2017,
total_trx_2018,
total_trx_2019,
total_trx_2020,
total_points_issued_2015,
total_points_issued_2016,
total_points_issued_2017,
total_points_issued_2018,
total_points_issued_2019,
total_points_issued_2020,
total_points_redeemed_2015,
total_points_redeemed_2016,
total_points_redeemed_2017,
total_points_redeemed_2018,
total_points_redeemed_2019,
total_points_redeemed_2020,


total_spend_201801,total_spend_201802,total_spend_201803,total_spend_201804,total_spend_201805,
total_spend_201806,total_spend_201807,total_spend_201808,total_spend_201809,total_spend_201810,
total_spend_201811,total_spend_201812,
total_spend_201901,
total_spend_201902,
total_spend_201903,
total_spend_201904,
total_spend_201905,
total_spend_201906,
total_spend_201907,
total_spend_201908,
total_spend_201909,
total_spend_201910,
total_spend_201911,
total_spend_201912,
total_spend_202001,
total_spend_202002,
total_spend_202003,
total_spend_202004,
total_spend_202005,
total_spend_202006,
total_spend_202007,
total_spend_202008,
total_spend_202009,
total_spend_202010,
total_spend_202011,
total_spend_202012,
spend_l12m,
trx_l12m,
total_points_issued_l12m,
total_points_redeemed_l12m,
spend_l6m,
trx_l6m,
total_points_issued_l6m,
total_points_redeemed_l6m,
spend_l3m,
trx_l3m,
total_points_issued_l3m,
total_points_redeemed_l3m,
spend_l1m,
trx_l1m,
total_points_issued_l1m,
total_points_redeemed_l1m,
spend_f1m,
trx_f1m,
total_points_issued_f1m,
total_points_redeemed_f1m,
spend_f6m,
trx_f6m,
total_points_issued_f6m,
total_points_redeemed_f6m,
count_month_l1m,
count_month_l3m,
count_month_l6m,
count_month_l12m,
date_of_acquisition,
yearmonth_of_acquisition,
store_code_of_acquisition,
store_description_of_acquisition,
store_group_of_acquisition,
business_unit_of_acquisition,
days_since_acquisition,
months_since_acquisition,
years_since_acquisition
from
#dat_spend_all A 
left join #dat_spend_nextm J on A.csn=J.csn
left join #dat_spend_yrwise B on A.csn=B.csn
left join #dat_spend_mnthwise X on A.csn=X.csn
left join #dat_spend_l12m C on A.csn=C.csn
left join #dat_spend_l6m D on A.csn=D.csn
left join #dat_spend_l3m E on A.csn=E.csn
left join #dat_spend_l1m F on A.csn=F.csn
left join #dat_spend_f1m G on A.csn=G.csn
left join #dat_spend_f6m H on A.csn=H.csn
left join #dat_acq I on A.csn=I.csn
);

select top 10 * from #spend_tab;
select count(*) from #spend_tab;

drop table #dat_spend_all;
drop table #dat_spend_yrwise;
drop table #dat_spend_l12m;
drop table #dat_spend_l6m;
drop table #dat_spend_l1m;
drop table #dat_spend_l3m;
drop table #dat_spend_f1m;
drop table #dat_spend_f6m;
drop table #dat_acq;


------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Create activity , order gap, recency features
------------------------------------------------------------------------------------------------------------------------------------------------------------------

----------------------------------- Time since last trx--------------------------------------------------
drop table if exists #temp;
create temp table #temp 
distkey(csn)
sortkey(csn)
as
(select *, row_number() over (partition by csn order by business_date desc) as row_num from #dat_trx_sumry);
select top 10 * from #temp;
select count(*) from #temp;
select count( distinct csn) from #temp;

drop table if exists #ts_last_trx;
create temp table #ts_last_trx 
distkey(csn)
sortkey(csn)
as
(select csn,business_date as date_of_last_trx,
(extract(year from date_of_last_trx)*100 +extract(month from date_of_last_trx)) as yearmonth_of_last_trx,
store_code as store_code_of_last_trx,
store_description as store_description_of_last_trx, 
business_unit as business_unit_of_last_trx,
datediff('days',date_of_last_trx,date_trunc('day',(select max(business_date) from #dat_trx))) as days_since_last_trx,
datediff('months',date_of_last_trx,date_trunc('month',(select max(business_date) from #dat_trx))) as months_since_last_trx,
datediff('years',date_of_last_trx,date_trunc('year',(select max(business_date) from #dat_trx))) as years_since_last_trx
from #temp where row_num=1 );

select top 10 * from #ts_last_trx;
select count(*) from #ts_last_trx;
select count( distinct csn) from #ts_last_trx;

------------------------------------------------ Time since 2nd last trx ----------------------------------------------------------------------
drop table if exists #ts_2ndlast_trx;
create temp table #ts_2ndlast_trx 
distkey(csn)
sortkey(csn)
as
(select csn,business_date as date_of_2nd_last_trx,
(extract(year from date_of_2nd_last_trx)*100 +extract(month from date_of_2nd_last_trx)) as yearmonth_of_2nd_last_trx,
store_code as store_code_of_2nd_last_trx,
store_description as store_description_of_2nd_last_trx, 
business_unit as business_unit_of_2nd_last_trx,
datediff('days',date_of_2nd_last_trx,date_trunc('day',(select max(business_date) from #dat_trx))) as days_since_2nd_last_trx,
datediff('months',date_of_2nd_last_trx,date_trunc('month',(select max(business_date) from #dat_trx))) as months_since_2nd_last_trx
from #temp where row_num=2 );

select top 10 * from #ts_2ndlast_trx;
select count(*) from #ts_2ndlast_trx;
select count( distinct csn) from #ts_2ndlast_trx;

---------------------------------------------------------- Inactivity status ----------------------------------------------------------------------

drop table if exists #inactivity_status;
create temp table #inactivity_status 
distkey(csn)
sortkey(csn)
as
(select csn,
case 
when days_since_last_trx<= 7  then '1)active_within_7days'
when days_since_last_trx>7 and days_since_last_trx<= 14 then '2)inactive_within_7_14days'
when days_since_last_trx>14 and days_since_last_trx<= 30 then '3)inactive_within_14_30days'
when days_since_last_trx>30 and days_since_last_trx<= 60 then '4)inactive_within_30_60days'
when days_since_last_trx>60 and days_since_last_trx<= 90 then '5)inactive_within_60_90days'
when days_since_last_trx>90 and days_since_last_trx<= 120 then '6)inactive_within_90_120days'
when days_since_last_trx>120  then '7)inactive_more_than_120_days'
else '0)active' end as activity_status_detailed
from #ts_last_trx );

select * from #inactivity_status  where activity_status_detailed is null;
select top 10 * from #inactivity_status;
select count(*) from #inactivity_status;
select count( distinct csn) from #inactivity_status;

---------------------------------------------------------------- time between first 5 trx -----------------------------------------------------------------
drop table if exists #trx1;
create temp table #trx1 
distkey(csn)
sortkey(csn)
as
(select csn, business_date as date_of_trx_1 from #trx where row_num=1);

drop table if exists #trx2;
create temp table #trx2
distkey(csn)
sortkey(csn)
as
(select csn, business_date as date_of_trx_2 from #trx where row_num=2);

drop table if exists #trx3;
create temp table #trx3 
distkey(csn)
sortkey(csn)
as
(select csn, business_date as date_of_trx_3 from #trx where row_num=3);

drop table if exists #trx4;
create temp table #trx4
distkey(csn)
sortkey(csn)
as
(select csn, business_date as date_of_trx_4 from #trx where row_num=4);

drop table if exists #trx5;
create temp table #trx5 
distkey(csn)
sortkey(csn)
as
(select csn, business_date as date_of_trx_5 from #trx where row_num=5);

------------------------------------------------------------- Activity period --------------------------------------------------------------------------------
drop table if exists #activity_period;
create temp table #activity_period 
distkey(csn)
sortkey(csn)
as
(select csn, 
datediff('days',min(business_date),max(business_date)) as activity_period_days,
(1.00*datediff('days',min(business_date),max(business_date)))/30 as activity_period_months,
(1.00*datediff('days',min(business_date),max(business_date)))/365 as activity_period_years
from #dat_trx_sumry
group by csn );
select top 10 * from #activity_period;

------------------------------------------------------------ Order gap features --------------------------------------------------------------------------------
drop table if exists #dat_order_gap;
create temp table #dat_order_gap 
distkey(csn)
sortkey(csn)
as
(select *, lag(business_date,1) over (partition by csn order by business_date) as invoice_date_lag1 from #dat_trx_sumry);
select top 10 * from #dat_order_gap;

drop table if exists #dat_order_gap_2;
create temp table #dat_order_gap_2
distkey(csn)
sortkey(csn) as
(select *, date_diff('days',invoice_date_lag1,business_date) as order_gap from #dat_order_gap);
select top 10 * from #dat_order_gap_2;

-------------------------------------------- Overall------------------------------------------------------------------------------------------
drop table if exists #dat_order_gap_all;
create temp table #dat_order_gap_all 
distkey(csn)
sortkey(csn)
as
(select csn, 
avg(order_gap) as avg_order_gap_all, 
PERCENTILE_CONT(0.5) WITHIN GROUP (ORDER BY order_gap) as medn_order_gap_all,
PERCENTILE_CONT(0.25) WITHIN GROUP (ORDER BY order_gap) as p25_order_gap_all,
PERCENTILE_CONT(0.75) WITHIN GROUP (ORDER BY order_gap) as p75_order_gap_all,
PERCENTILE_CONT(0.05) WITHIN GROUP (ORDER BY order_gap) as p05_order_gap_all,
PERCENTILE_CONT(0.95) WITHIN GROUP (ORDER BY order_gap) as p95_order_gap_all,
stddev(order_gap) as std_order_gap_all
from #dat_order_gap_2
--where order_gap is not null
where order_gap >0
group by csn );
select top 10 * from #dat_order_gap_all;


------------------------------------------------------------- last 6M -----------------------------------------------------------------------------------
drop table if exists #dat_order_gap_L6M;
create temp table #dat_order_gap_L6M 
distkey(csn)
sortkey(csn)
as
(select csn, 
avg(order_gap) as avg_order_gap_l6m, 
PERCENTILE_CONT(0.5) WITHIN GROUP (ORDER BY order_gap) as medn_order_gap_l6m,
PERCENTILE_CONT(0.25) WITHIN GROUP (ORDER BY order_gap) as p25_order_gap_l6m,
PERCENTILE_CONT(0.75) WITHIN GROUP (ORDER BY order_gap) as p75_order_gap_l6m,
PERCENTILE_CONT(0.05) WITHIN GROUP (ORDER BY order_gap) as p05_order_gap_l6m,
PERCENTILE_CONT(0.95) WITHIN GROUP (ORDER BY order_gap) as p95_order_gap_l6m,
stddev(order_gap) as std_order_gap_l6m
from #dat_order_gap_2
--where order_gap is not null and 
where order_gap >0 and
datediff('months',business_date,DATEADD(month, -1, date_trunc('month',(select max(business_date) from #dat_trx))))<=6
group by csn );
select top 10 * from #dat_order_gap_L6M;

--select * from #dat_order_gap_L6M where csn='000192bbf77f96bcb09e76d09068d15c7cf67468';
--select * from #dat_order_gap_all where csn='000192bbf77f96bcb09e76d09068d15c7cf67468';

--------------------------------------- Median deviation from median order gap overall ---------------------------------------------------------

drop table if exists #dat_order_gap_mad;
create temp table #dat_order_gap_mad
distkey(csn)
sortkey(csn)
as
(select A.csn, A.order_gap, B.medn_order_gap_all , 
abs(A.order_gap - B.medn_order_gap_all) as deviation
from #dat_order_gap_2 A left join #dat_order_gap_all B on A.csn=B.csn
where order_gap >0);
select top 10 * from #dat_order_gap_mad;

drop table if exists #dat_order_gap_mad_2;
create temp table #dat_order_gap_mad_2
distkey(csn)
sortkey(csn)
as
(select csn, 
PERCENTILE_CONT(0.5) WITHIN GROUP (ORDER BY deviation) as mad_order_gap_all
from #dat_order_gap_mad
group by csn );
select top 10 * from #dat_order_gap_mad_2;


--------------------------------------------------- Median deviation from median order gap last 6M -----------------------------------------------------------
drop table if exists #dat_order_gap_mad_l6m;
create temp table #dat_order_gap_mad_l6m
distkey(csn)
sortkey(csn)
as
(select 
A.csn, A.order_gap, B.medn_order_gap_l6m , 
abs(A.order_gap - B.medn_order_gap_l6m) as deviation
from #dat_order_gap_2 A left join #dat_order_gap_l6m B on A.csn=B.csn 
where datediff('months',business_date,sysdate)<=6 and order_gap >0
);
select top 10 * from #dat_order_gap_mad_l6m;

drop table if exists #dat_order_gap_mad_l6m_2;
create temp table #dat_order_gap_mad_l6m_2
distkey(csn)
sortkey(csn)
as
(select csn, 
PERCENTILE_CONT(0.5) WITHIN GROUP (ORDER BY deviation) as mad_order_gap_l6m 
from #dat_order_gap_mad_l6m
group by csn );
select top 10 * from #dat_order_gap_mad_l6m_2;

------------------------------------------------------------- Trimmed mean of order gap -----------------------------------------------------------------

------------------------------------------------------- Overall ----------------------------------------------------------------------

drop table if exists #dat_order_gap_tr;
create temp table #dat_order_gap_tr 
distkey(csn)
sortkey(csn)
as
(select A.csn, A.order_gap, B.p05_order_gap_all,B.p95_order_gap_all
from #dat_order_gap_2 A left join #dat_order_gap_all B on A.csn=B.csn
where order_gap >0);
select top 10 * from #dat_order_gap_tr;
select count(*) from #dat_order_gap_tr ;

drop table if exists #dat_order_gap_tr_2;
create temp table #dat_order_gap_tr_2 
distkey(csn)
sortkey(csn)
as
(select csn,avg(trimmed_order_gap) as trimmed_order_gap_all from
(select csn, 
case when order_gap < p05_order_gap_all then p05_order_gap_all when order_gap > p95_order_gap_all then p95_order_gap_all else order_gap end as trimmed_order_gap
from #dat_order_gap_tr 
--where order_gap is not null
where order_gap >0)
group by csn);
select top 10 * from #dat_order_gap_tr_2;


------------------------------------------------------------ Last 6m ----------------------------------------------------------------------------

drop table if exists #dat_order_gap_tr_l6m;
create temp table #dat_order_gap_tr_l6m 
distkey(csn)
sortkey(csn)
as
(select A.csn, A.order_gap, B.p05_order_gap_l6m,B.p95_order_gap_l6m
from #dat_order_gap_2 A left join #dat_order_gap_l6m B on A.csn=B.csn
where datediff('months',business_date,sysdate)<=6 and order_gap >0);
select top 10 * from #dat_order_gap_tr_l6m;
select count(*) from #dat_order_gap_tr_l6m ;

drop table if exists #dat_order_gap_tr_l6m_2;
create temp table #dat_order_gap_tr_l6m_2 
distkey(csn)
sortkey(csn)
as
(select csn,avg(trimmed_order_gap) as trimmed_order_gap_l6m from
(select csn, 
case when order_gap < p05_order_gap_l6m then p05_order_gap_l6m when order_gap > p95_order_gap_l6m then p95_order_gap_l6m else order_gap end as trimmed_order_gap
from #dat_order_gap_tr_l6m 
--where order_gap is not null
where order_gap >0)
group by csn);
select top 10 * from #dat_order_gap_tr_l6m_2;

-----------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------ Reactivity status -----------------------------------------------------------------------------
drop table if exists #dat_reactivity;
create temp table #dat_reactivity 
distkey(csn)
sortkey(csn)
as
(select csn, 
sum(case when order_gap> 30 and order_gap<= 60 then 1 else 0 end) as count_30_60_day_reactive,
sum(case when order_gap> 60 and order_gap<= 90 then 1 else 0 end) as count_60_90_day_reactive,
sum(case when order_gap> 90 and order_gap<= 120 then 1 else 0 end) as count_90_120_day_reactive,
sum(case when order_gap> 120 and order_gap<= 180 then 1 else 0 end) as count_120_180_day_reactive,
sum(case when order_gap> 180 then 1 else 0 end) as count_180_day_and_more_reactive,
sum(case when order_gap> 30 then 1 else 0 end) as count_lapse_30days,
sum(case when order_gap> 60 then 1 else 0 end) as count_lapse_60days,
sum(case when order_gap> 90 then 1 else 0 end) as count_lapse_90days
from #dat_order_gap_2
group by csn);
select top 10 * from #dat_reactivity;

------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Merge in all recency features
------------------------------------------------------------------------------------------------------------------------------------------------------------------

drop table if exists #dat_recency;
create temp table #dat_recency
distkey(csn)
sortkey(csn)
as
(select 
A.csn,
date_of_last_trx,
yearmonth_of_last_trx,
store_code_of_last_trx,
store_description_of_last_trx,
business_unit_of_last_trx,
days_since_last_trx,
months_since_last_trx,
years_since_last_trx,
date_of_2nd_last_trx,
yearmonth_of_2nd_last_trx,
store_code_of_2nd_last_trx,
store_description_of_2nd_last_trx,
business_unit_of_2nd_last_trx,
days_since_2nd_last_trx,
months_since_2nd_last_trx,
activity_status_detailed,
date_of_trx_1,
date_of_trx_2,
date_of_trx_3,
date_of_trx_4,
date_of_trx_5,
activity_period_days,
activity_period_months,
activity_period_years,
avg_order_gap_all,
medn_order_gap_all,
p25_order_gap_all,
p75_order_gap_all,
p05_order_gap_all,
p95_order_gap_all,
std_order_gap_all,
avg_order_gap_l6m,
medn_order_gap_l6m,
p25_order_gap_l6m,
p75_order_gap_l6m,
p05_order_gap_l6m,
p95_order_gap_l6m,
std_order_gap_l6m,
mad_order_gap_all,
mad_order_gap_l6m,
trimmed_order_gap_all,
trimmed_order_gap_l6m,
count_30_60_day_reactive,
count_60_90_day_reactive,
count_90_120_day_reactive,
count_120_180_day_reactive,
count_180_day_and_more_reactive,
count_lapse_30days,
count_lapse_60days,
count_lapse_90days
from 
#ts_last_trx A 
left join #ts_2ndlast_trx B on A.csn=B.csn
left join #inactivity_status C on A.csn=C.csn
left join #trx1 D on A.csn=D.csn
left join #trx2 E on A.csn=E.csn
left join #trx3 F on A.csn=F.csn
left join #trx4 G on A.csn=G.csn
left join #trx5 H on A.csn=H.csn
left join #activity_period I on A.csn=I.csn
left join #dat_order_gap_all J on A.csn=J.csn
left join #dat_order_gap_L6M K on A.csn=K.csn
left join #dat_order_gap_tr_2 L on A.csn=L.csn
left join #dat_order_gap_tr_l6m_2 M on A.csn=M.csn
left join #dat_reactivity N on A.csn=N.csn
left join #dat_order_gap_mad_2 O on A.csn=O.csn
left join #dat_order_gap_mad_l6m_2 P on A.csn=P.csn);

select top 10 * from #dat_recency;
select count(*) from #dat_recency;
select count( distinct csn) from #dat_recency;

-- drop tables
drop table #ts_last_trx;
drop table #ts_2ndlast_trx;
drop table #inactivity_status;
drop table #dat_order_gap_all;
drop table #dat_order_gap_L6M;
drop table #dat_order_gap_tr_2;
drop table #dat_order_gap_tr_l6m_2;
drop table #dat_reactivity;
drop table #activity_period;
drop table #dat_order_gap_mad_2;
drop table #dat_order_gap_mad_l6m_2;
drop table #dat_order_gap_tr;
drop table #dat_order_gap_tr_l6m;
drop table #dat_order_gap_mad;
drop table #dat_order_gap_mad_l6m;

---------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Merge spend and recency features
---------------------------------------------------------------------------------------------------------------------------------------------------------------------

drop table if exists #dat_spend_recency;
create table #dat_spend_recency as
(select 
A.*,
date_of_last_trx,
yearmonth_of_last_trx,
store_code_of_last_trx,
store_description_of_last_trx,
business_unit_of_last_trx,
days_since_last_trx,
months_since_last_trx,
years_since_last_trx,
date_of_2nd_last_trx,
yearmonth_of_2nd_last_trx,
store_code_of_2nd_last_trx,
store_description_of_2nd_last_trx,
business_unit_of_2nd_last_trx,
days_since_2nd_last_trx,
months_since_2nd_last_trx,
activity_status_detailed,
date_of_trx_1,
date_of_trx_2,
date_of_trx_3,
date_of_trx_4,
date_of_trx_5,
activity_period_days,
activity_period_months,
activity_period_years,
avg_order_gap_all,
medn_order_gap_all,
p25_order_gap_all,
p75_order_gap_all,
p05_order_gap_all,
p95_order_gap_all,
std_order_gap_all,
avg_order_gap_l6m,
medn_order_gap_l6m,
p25_order_gap_l6m,
p75_order_gap_l6m,
p05_order_gap_l6m,
p95_order_gap_l6m,
std_order_gap_l6m,
mad_order_gap_all,
mad_order_gap_l6m,
trimmed_order_gap_all,
trimmed_order_gap_l6m,
count_30_60_day_reactive,
count_60_90_day_reactive,
count_90_120_day_reactive,
count_120_180_day_reactive,
count_180_day_and_more_reactive,
count_lapse_30days,
count_lapse_60days,
count_lapse_90days
from #spend_tab A left join #dat_recency B on A.csn=B.csn);

select top 10 * from #dat_spend_recency;
drop table #spend_tab;
drop table #dat_recency;


-- compute time intervals
drop table if exists #dat_timeint;
create temp table #dat_timeint
distkey(csn)
sortkey(csn)
as
(select *,
datediff('days',date_of_acquisition,date_of_trx_2) as days_btw_1_2_trx,
datediff('days',date_of_acquisition,date_of_trx_3) as days_btw_1_3_trx,
datediff('days',date_of_acquisition,date_of_trx_4) as days_btw_1_4_trx,
datediff('days',date_of_acquisition,date_of_trx_5) as days_btw_1_5_trx,
datediff('days',date_of_trx_2,date_of_trx_3) as days_btw_2_3_trx,
datediff('days',date_of_trx_3,date_of_trx_4) as days_btw_3_4_trx,
datediff('days',date_of_trx_4,date_of_trx_5) as days_btw_4_5_trx
from #dat_spend_recency);

select top 10 * from #dat_timeint;
select count(*) from #dat_timeint;
select count( distinct csn) from #dat_timeint;


-- create monthly streak features

drop table if exists #dat_trx_frq_1;
create temp table #dat_trx_frq_1 
distkey(csn)
sortkey(csn)
as
(
select csn,yearid,monthid,year_month,
sum(total_spend) as total_spend,
count(distinct trx_code) as count_trx
from #dat_trx_sumry
where total_spend>0 
group by csn,yearid,monthid,year_month);
select top 10 * from #dat_trx_frq_1;

drop table if exists #dat_trx_frq_2;
create temp table #dat_trx_frq_2
distkey(csn)
sortkey(csn)
as
(select *, 
lag(year_month,1) over (partition by csn order by year_month) as lag_year_month_1,
lag(year_month,2) over (partition by csn order by year_month) as lag_year_month_2,
lag(year_month,3) over (partition by csn order by year_month) as lag_year_month_3
from #dat_trx_frq_1);
select top 10 * from #dat_trx_frq_2;

drop table if exists #dat_trx_frq_3;
create temp table #dat_trx_frq_3
distkey(csn)
sortkey(csn)
as
(select *, 
case 
when year_month/100=lag_year_month_1/100 then (year_month - lag_year_month_1) 
when year_month/100>lag_year_month_1/100 then (mod(year_month,100) + 12 - mod(lag_year_month_1,100)) end as diff_lag_1,
case 
when year_month/100=lag_year_month_2/100 then (year_month - lag_year_month_2) 
when year_month/100>lag_year_month_2/100 then (mod(year_month,100) + 12 - mod(lag_year_month_2,100)) end as diff_lag_2
from #dat_trx_frq_2);
select * from #dat_trx_frq_3 where csn='000192bbf77f96bcb09e76d09068d15c7cf67468';

-- count back to back 3 months visits
drop table if exists #dat_trx_frq_4;
create temp table #dat_trx_frq_4
distkey(csn)
sortkey(csn)
as
(select csn, count(case when diff_lag_1=1 and diff_lag_2=2 then 1 else 0 end) as count_3M_back2back from #dat_trx_frq_3 group by csn);

select * from #dat_trx_frq_4 where csn='000192bbf77f96bcb09e76d09068d15c7cf67468';
select top 10 * from #dat_trx_frq_4 ;

-- count back to back 2 months visits
drop table if exists #dat_trx_frq_4_1;
create temp table #dat_trx_frq_4_1
distkey(csn)
sortkey(csn)
as
(select csn, count(case when diff_lag_1=1 then 1 else 0 end) as count_2M_back2back from #dat_trx_frq_3 group by csn);
select * from #dat_trx_frq_4_1 where csn='000192bbf77f96bcb09e76d09068d15c7cf67468';
select top 10 * from #dat_trx_frq_4_1 ;


-- median monthly trx
drop table if exists #dat_trx_frq_5;
create temp table #dat_trx_frq_5
distkey(csn)
sortkey(csn)
as
(select csn,
min(count_trx) as min_monthly_trx,
max(count_trx) as max_monthly_trx,
stddev(count_trx) as std_monthly_trx,
count(distinct year_month) as count_month_active
from #dat_trx_frq_1 group by csn);

select * from #dat_trx_frq_5 where csn='000192bbf77f96bcb09e76d09068d15c7cf67468';
select top 10 * from #dat_trx_frq_5 ;


-- counts months with 1,2,3,4,5 and more than 5 trx
drop table if exists #dat_trx_frq_6;
create temp table #dat_trx_frq_6
distkey(csn)
sortkey(csn)
as
(select csn, year_month,
case when count_trx=1 then 1 else 0 end as trx_1,
case when count_trx=2 then 1 else 0 end as trx_2,
case when count_trx=3 then 1 else 0 end as trx_3,
case when count_trx=4 then 1 else 0 end as trx_4,
case when count_trx=5 then 1 else 0 end as trx_5,
case when count_trx>5 then 1 else 0 end as trx_6_and_more
from #dat_trx_frq_1);
select * from #dat_trx_frq_6 where csn='000192bbf77f96bcb09e76d09068d15c7cf67468';
select top 10 * from #dat_trx_frq_6 ;

drop table if exists #dat_trx_frq_7;
create temp table #dat_trx_frq_7
distkey(csn)
sortkey(csn)
as
(select csn, sum(trx_1) as count_month_trx_1,
sum(trx_2) as count_month_trx_2,
sum(trx_3) as count_month_trx_3,
sum(trx_4) as count_month_trx_4,
sum(trx_5) as count_month_trx_5,sum(trx_6_and_more) as count_month_trx_6_and_more
from #dat_trx_frq_6 group by csn);

select * from #dat_trx_frq_7 where csn='000192bbf77f96bcb09e76d09068d15c7cf67468';
select top 10 * from #dat_trx_frq_7 ;

---------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Merge spend,recency and frequency features
---------------------------------------------------------------------------------------------------------------------------------------------------------------------

drop table if exists #dat_spend_recency_frq;
create temp table #dat_spend_recency_frq
distkey(csn)
sortkey(csn)
as
(select 
A.*,
count_3m_back2back,
count_2m_back2back,
min_monthly_trx,
max_monthly_trx,
std_monthly_trx,
count_month_active,
count_month_trx_1,
count_month_trx_2,
count_month_trx_3,
count_month_trx_4,
count_month_trx_5,
count_month_trx_6_and_more
from
#dat_timeint A 
left join #dat_trx_frq_4 B on A.csn=B.csn
left join #dat_trx_frq_4_1 C on A.csn=C.csn
left join #dat_trx_frq_5 D on A.csn=D.csn
left join #dat_trx_frq_7 E on A.csn=E.csn);

select top 10 * from #dat_spend_recency_frq;
select * from #dat_spend_recency_frq where csn='000088609cc5aac77909b8d1a7aaeea14a363d41';

--drop tables
drop table #dat_timeint;
drop table #dat_trx_frq_1;
drop table #dat_trx_frq_2;
drop table #dat_trx_frq_3;
drop table #dat_trx_frq_4;
drop table #dat_trx_frq_4_1;
drop table #dat_trx_frq_5;
drop table #dat_trx_frq_6;
drop table #dat_trx_frq_7;

-- create some more frequency and basket size features

drop table if exists #dat_spend_recency_frq_2;
create table #dat_spend_recency_frq_2 as 
(
select *,
NVL(spend_l12m/nullif(trx_l12m,0),0) as basket_size_l12m,
NVL((spend_l6m/nullif(trx_l6m,0)),0) as basket_size_l6m,
NVL(spend_l3m/nullif(trx_l3m,0),0) as basket_size_l3m,
NVL(spend_l1m/nullif(trx_l1m,0),0) as basket_size_l1m,
NVL(count_month_active/nullif(activity_period_months,0),0) as active_rate,
NVL(trx_l12m/12,0) as trx_perm_l12m,
NVL(trx_l6m/6,0) as trx_perm_l6m,
NVL(trx_l3m/3,0) as trx_perm_l3m,
NVL(trx_l1m/1,0) as trx_perm_l1m,
NVL(trx_l12m/nullif(count_month_l12m,0),0) as trx_peram_l12m,
NVL(trx_l6m/nullif(count_month_l6m,0),0) as trx_peram_l6m,
NVL(trx_l3m/nullif(count_month_l3m,0),0) as trx_peram_l3m,
NVL(trx_l1m/nullif(count_month_l1m,0),0) as trx_peram_l1m,
NVL(spend_l12m/12,0) as spend_perm_l12m,
NVL(spend_l6m/6,0) as spend_perm_l6m,
NVL(spend_l3m/3,0) as spend_perm_l3m,
NVL(spend_l1m/1,0) as spend_perm_l1m,
NVL(spend_l12m/nullif(count_month_l12m,0),0) as spend_peram_l12m,
NVL(spend_l6m/nullif(count_month_l6m,0),0) as spend_peram_l6m,
NVL(spend_l3m/nullif(count_month_l3m,0),0) as spend_peram_l3m,
NVL(spend_l1m/nullif(count_month_l1m,0),0) as spend_peram_l1m
from #dat_spend_recency_frq);

select top 10 * from #dat_spend_recency_frq_2;
select * from #dat_spend_recency_frq_2 where csn='000088609cc5aac77909b8d1a7aaeea14a363d41';


-- create some more features

drop table if exists #dat_spend_recency_frq_3;
create table #dat_spend_recency_frq_3 as 
(
select *,
100*NVL((basket_size_l6m-basket_size_l12m)/nullif(basket_size_l12m,0),0) as perc_chg_basket_size_lm_12_6,
100*NVL((basket_size_l3m-basket_size_l6m)/nullif(basket_size_l6m,0),0) as perc_chg_basket_size_lm_6_3,
100*NVL((basket_size_l1m-basket_size_l3m)/nullif(basket_size_l3m,0),0) as perc_chg_basket_size_lm_3_1,

100*NVL((trx_perm_l6m-trx_perm_l12m)/nullif(trx_perm_l12m,0),0) as perc_chg_trx_perm_lm_12_6,
100*NVL((trx_perm_l3m-trx_perm_l6m)/nullif(trx_perm_l6m,0),0) as perc_chg_trx_perm_lm_6_3,
100*NVL((trx_perm_l1m-trx_perm_l3m)/nullif(trx_perm_l3m,0),0) as perc_chg_trx_perm_lm_3_1,

100*NVL((trx_peram_l6m-trx_peram_l12m)/nullif(trx_peram_l12m,0),0) as perc_chg_trx_peram_lm_12_6,
100*NVL((trx_peram_l3m-trx_peram_l6m)/nullif(trx_peram_l6m,0),0) as perc_chg_trx_peram_lm_6_3,
100*NVL((trx_peram_l1m-trx_peram_l3m)/nullif(trx_peram_l3m,0),0) as perc_chg_trx_peram_lm_3_1,

100*NVL((spend_perm_l6m-spend_perm_l12m)/nullif(spend_perm_l12m,0),0) as perc_chg_spend_perm_lm_12_6,
100*NVL((spend_perm_l3m-spend_perm_l6m)/nullif(spend_perm_l6m,0),0) as perc_chg_spend_perm_lm_6_3,
100*NVL((spend_perm_l1m-spend_perm_l3m)/nullif(spend_perm_l3m,0),0) as perc_chg_spend_perm_lm_1_3,

100*NVL((spend_peram_l6m-spend_peram_l12m)/nullif(spend_peram_l12m,0),0) as perc_chg_spend_peram_lm_12_6,
100*NVL((spend_peram_l3m-spend_peram_l6m)/nullif(spend_peram_l6m,0),0) as perc_chg_spend_peram_lm_6_3,
100*NVL((spend_peram_l1m-spend_peram_l3m)/nullif(spend_peram_l3m,0),0) as perc_chg_spend_peram_lm_1_3,

100*count_month_l12m/12 as active_rate_l12m,
100*count_month_l6m/6 as active_rate_l6m,
100*count_month_l3m/3 as active_rate_l3m,
100*count_month_l1m/1 as active_rate_l1m
from #dat_spend_recency_frq_2);
select top 10 * from #dat_spend_recency_frq_3;

-- add in the distance features
select top 10 * from lac.customer_distance_based_metrics;

drop table if exists #dat_spend_recency_frq_4;
create table #dat_spend_recency_frq_4 as 
(select 
A.*,
nearest_fp_branch_format,
nearest_fp_branch_dist_miles,
count_fp_within_1m,
count_fp_within_2m,
count_fp_within_3m,
count_finest_within_1m,
count_finest_within_2m,
count_finest_within_3m,
count_hypermarket_within_1m,
count_hypermarket_within_2m,
count_hypermarket_within_3m,
count_supermarket_within_1m,
count_supermarket_within_2m,
count_supermarket_within_3m,
nearest_cs_dist_miles,
nearest_pr_dist_miles,
nearest_ss_dist_miles,
count_cs_within_1m,
count_cs_within_2m,
count_cs_within_3m,
count_ss_within_1m,
count_ss_within_2m,
count_ss_within_3m,
count_pr_within_1m,
count_pr_within_2m,
count_pr_within_3m 
from #dat_spend_recency_frq_3 A left join lac.customer_distance_based_metrics B
on A.csn = B.csn);
select top 10 * from #dat_spend_recency_frq_4;

----------------------Create edm action features-----------------------
drop table if exists #campaign_fp;
create table #campaign_fp as(
select 
		id_campaign,
		csn,
		max(case when event='sent' then 1 else 0 end) as sent,
		max(case when event='delivered' then 1 else 0 end) as delivered,
		max(case when event='open' then 1 else 0 end) as opened,
		max(case when event='click' then 1 else 0 end) as clicked,
		max(case when event='unsub' then 1 else 0 end) as unsub
	from edw.fact_sfmc_comms_metrics
	where event in ('sent', 'delivered', 'open', 'click')
	  and datediff('months',event_date, getdate())<=3
		and id_campaign in (select DISTINCT email_id from sfmc.gsheet_marketing_metadata 
where merchant_name_1 ilike '%FAIRPRICE%')
group by 1, 2);

--select top 10* from #campaign_fp;

drop table if exists #campaign_fp_rate;
create table #campaign_fp_rate as(
select csn, 
       sum(delivered) as sum_delivered,
       sum(opened) as sum_opened,
       sum(clicked) as sum_clicked,
       (case when sum(delivered)>0 then round(convert(float,sum_opened)/convert(float,sum_delivered),3) else 0 end) as open_rate_fp,
       (case when sum(delivered)>0 then round(convert(float,sum_clicked)/convert(float,sum_delivered),3) else 0 end) as click_rate_fp,
       max(unsub) as unsub_fp
from #campaign_fp
group by csn);
--select top 10* from #campaign_fp_rate;


drop table if exists #campaign_link;
create table #campaign_link as(
select 
		id_campaign,
		csn,
		max(case when event='sent' then 1 else 0 end) as sent,
		max(case when event='delivered' then 1 else 0 end) as delivered,
		max(case when event='open' then 1 else 0 end) as opened,
		max(case when event='click' then 1 else 0 end) as clicked,
		max(case when event='unsub' then 1 else 0 end) as unsub
	from edw.fact_sfmc_comms_metrics
	where event in ('sent', 'delivered', 'open', 'click')
	  and datediff('months',event_date, getdate())<=3
		and id_campaign in (select DISTINCT email_id from sfmc.gsheet_marketing_metadata 
where merchant_name_1 ilike '%LINK%')
group by 1, 2);

--select top 10* from #campaign_link;

drop table if exists #campaign_link_rate;
create table #campaign_link_rate as(
select csn,
       sum(delivered) as sum_delivered,
       sum(opened) as sum_opened,
       sum(clicked) as sum_clicked,
       (case when sum(delivered)>0 then round(convert(float,sum_opened)/convert(float,sum_delivered),3) else 0 end) as open_rate_link,
       (case when sum(delivered)>0 then round(convert(float,sum_clicked)/convert(float,sum_delivered),3) else 0 end) as click_rate_link,
       max(unsub) as unsub_link
from #campaign_link
group by csn);
--select top 10* from #campaign_link_rate;

---------------Create vintage feature--------------------------
drop table if exists #vintage_link;
create table #vintage_link as(
select csn, datediff('months',min(transaction_date),sysdate) as vintage_link
from link.view_facttrans_plus
group by csn);


drop table if exists #vintage_fp;
create table #vintage_fp as(
select csn, datediff('months',min(transaction_date),sysdate) as vintage_fp
from link.view_facttrans_plus
WHERE corporation_id in ('FP')
group by csn);


-------------Merge #dat_spend_recency_frq_4 with edm and vintage features-------------
drop table if exists #table_final;
create table #table_final as(
select A.*, open_rate_fp,click_rate_fp,unsub_fp,open_rate_link,click_rate_link,unsub_link,vintage_link,vintage_fp
from #dat_spend_recency_frq_4 A
left join #campaign_fp_rate B on A.csn=B.csn
left join #campaign_link_rate C on A.csn=C.csn
left join #vintage_link D on A.csn=D.csn
left join #vintage_fp E on A.csn=E.csn);

--select * from #table_final limit 100;

drop table if exists lac.FP_cloud_customer_360_new_202008;
select getdate() as date_modified,* into lac.FP_cloud_customer_360_new_202008 from #table_final ;
select top 10 * from lac.FP_cloud_customer_360_new_202008 ;
select count(distinct csn) from lac.FP_cloud_customer_360_new_202008 ;
drop table #dat_spend_recency_frq;
drop table #dat_spend_recency_frq_2;
drop table #dat_spend_recency_frq_3;
drop table #dat_spend_recency_frq_4;

grant all on lac.FP_cloud_customer_360_new_202008 to taoxinru, milounidesai;

