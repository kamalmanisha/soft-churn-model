
# coding: utf-8

file_prefix= "FP_Churn_OG"
file_prefix_old=""
# lapser_month_year
tmy = int(input("Please enter lapser month year(format-yyyymm): "))


lapser_month_year = tmy
#blast no
blast_no=1
method='xgboost_f1'

from FP_lapser_Engine_Helper_Functions import run_select_base_OG
from FP_lapser_Engine_Helper_Functions import create_model_train_data
from FP_lapser_Engine_Helper_Functions import process_model_train_data
from FP_lapser_Engine_Helper_Functions import train_xgboost
import sys
import time
timestr = time.strftime("%Y%m%d-%H%M%S")
sys.stdout = open(file_prefix+timestr+".txt", "w")

print("############################################################################")
print("############################################################################")
print("################## Training ##################################")    
print("############################################################################")
print("############################################################################")


print("############################################################################")
print("lapser Month :",lapser_month_year)
print("Blast No. :",blast_no)    
# print("Blast Date :",lapser_date_long)  
# print("Score threshold cutoff :",decile_cutoff)    

print("############################################################################")
print("\n")
print("############################################################################")
print("Starting Base file creation")
print("############################################################################")
#print("\n")

# create base data
run_select_base_OG(lapser_month_year,file_prefix,blast_no)

print("############################################################################")
print("Base file creation completed")
print("############################################################################")
print("\n")
print("############################################################################")
print("Starting lapser Score file creation ...")
print("############################################################################")
#print("\n")

print("############################################################################")
print("Base file creation completed")
print("############################################################################")
print("\n")
print("############################################################################")
print("Starting Modeling data creation ...")
print("############################################################################")
#print("\n")

# create model training data
create_model_train_data(lapser_month_year,file_prefix,blast_no)

print("############################################################################")
print("Modeling data creation complete...")
print("############################################################################")
#print("\n")

print("############################################################################")
print("Starting Modeling data pre processing ...")
print("############################################################################")
#print("\n")

# create model training data
process_model_train_data(file_prefix,blast_no,lapser_month_year)

print("############################################################################")
print("Starting Modeling  ...")
print("############################################################################")
#print("\n")

train_xgboost(file_prefix,blast_no,lapser_month_year,method)


print("############################################################################")
print("completed ...")
print("############################################################################")
print("\n")


