# coding: utf-8

# In[98]:


import glob
import os
import s3fs
import psycopg2
import pytz
import math
import random
import logging
import numpy as np
import datetime as dt, time
import pandas as pd 
from sklearn import metrics
from pytz import timezone
from scipy.stats import norm
from sklearn.metrics import precision_recall_curve
from sklearn.metrics import f1_score
from sklearn.metrics import auc
from sklearn.metrics import average_precision_score
from matplotlib import pyplot as plt
from scipy import stats

con=psycopg2.connect("dbname= redshift01 host=nonprod-analytics-redshit01.cvdlsqkayfsg.ap-southeast-1.redshift.amazonaws.com port= 5439 user=milounidesai  password=xZ765MilDes89")    
cur=con.cursor()
    
def run_select_base_OG(lapser_month_year,file_prefix,blast_no):

    lapser_year = int(lapser_month_year/100)
    lapser_month = int(lapser_month_year%100)

    # Step 1: Check customers who were active M0,M1,M2
    if lapser_month == 1:
        month_L3 = [10,11,12]
        year_L3 = [lapser_year-1,lapser_year-1,lapser_year-1]
    elif lapser_month == 2:
        month_L3 = [11,12,1]
        year_L3 = [lapser_year-1,lapser_year-1,lapser_year] 
    elif lapser_month == 3:
        month_L3 = [12,1,2]
        year_L3 = [lapser_year-1,lapser_year,lapser_year]         
    elif lapser_month >= 4:
        month_L3 = [lapser_month-3,lapser_month-2,lapser_month-1]
        year_L3 = [lapser_year,lapser_year,lapser_year]

    M0_year_month= (year_L3[0]*100)+(month_L3[0])
    M1_year_month= (year_L3[1]*100)+(month_L3[1])
    M2_year_month= (year_L3[2]*100)+(month_L3[2])

    cur.execute("select distinct csn from lac.FP_cloud_customer_360_new_"+str(lapser_month_year)+" where (total_spend_"+str(M0_year_month)+" >0 or total_spend_"+str(M1_year_month)+" >0 or total_spend_"+str(M2_year_month)+" >0)")
    active_csn_df_3M = pd.DataFrame(cur.fetchall())
    field_names = [p[0] for p in cur.description]
    active_csn_df_3M.columns = field_names

    print("2 - No. of customers active in any of past 3 months:",active_csn_df_3M.shape[0])

    

    # Write out base file
    active_csn_df_3M.to_csv(file_prefix+"_"+str(blast_no)+"_"+"Base_Customer_List"+"_"+str(lapser_month_year)+".csv", index=False)

    # print base file checks
    print("\n")
    #print("Base file dim :",active_csn_df.shape)
    #print("Base file columns :",active_csn_df.columns)
    print("\n")

    return(1)

   

def update_ltd(lapser_month_year,file_prefix,method,blast_no,lapser_date_long):
   
    # create lapser date dataframe
    lapser_year = int(lapser_date_long/10000)
    lapser_month = int((lapser_date_long/100)%100)
    lapser_day =  int(lapser_date_long%100)
        
    #create lapser date
    lapser_date = dt.datetime(lapser_year,lapser_month, lapser_day)
   
    # Read in base file
    base_df= pd.read_csv(file_prefix+"_"+str(blast_no)+"_"+"Base_Customer_List"+"_"+str(lapser_month_year)+".csv")
    active_csn_df = pd.DataFrame() 
    active_csn_df['csn']=base_df['csn'].astype(int)
    print("No. of customers current month" +str(lapser_date.date())+" : "+str(active_csn_df.shape[0]))

    # Step 5 : Read FP Customer 360 data to get customer behavior features
    cur.execute('select csn,basket_size_l3m from lac.FP_cloud_customer_360_new_'+str(lapser_month_year))    
    FP_df= pd.DataFrame(cur.fetchall())
    field_names = [k[0] for k in cur.description]
    FP_df.columns = field_names
    active_csn_df['csn']=active_csn_df['csn'].astype(int)
    FP_df['csn']=FP_df['csn'].astype(int)
    active_csn_df = active_csn_df.merge(FP_df,on='csn',how='left')

    # Step 6 : Get last trx date for all customers
    cur.execute('select csn,max(business_date::date) as date_of_last_trx from fp.fp_sale_head A '\
                'left join fp.fp_store B on A.store_code=B.store_code '\
                'left join (select csn, card_no, func_sha1(card_no) as h_card_no from link.link_cls_card) C '\
                'on C.h_card_no=A.loyalty_card_no where csn is not null and store_group in '\
                " ('FAIRPRICE','FINEST','HYPERMARKET','HABA','WHC') and sale_net_val>0 and "\
                "business_date < cast('"+str(lapser_date.date())+"' as date) group by csn ;")
    Last_trx_df= pd.DataFrame(cur.fetchall())
    field_names = [k[0] for k in cur.description]
    Last_trx_df.columns = field_names
    active_csn_df['csn']=active_csn_df['csn'].astype(int)
    Last_trx_df['csn']=Last_trx_df['csn'].astype(int)    
    active_csn_df = active_csn_df.merge(Last_trx_df,on="csn", how="left")
    
    # Step 6 : Merge lapser date file
    active_csn_df['lapser_date_'+str(blast_no)]=pd.to_datetime(lapser_date)
    active_csn_df['date_of_last_trx']=pd.to_datetime(active_csn_df['date_of_last_trx'])

    # Step 7 - Compute the time since last trx
    active_csn_df['lapser_month_year']=lapser_month_year
    active_csn_df['days_since_last_trx']= (active_csn_df['lapser_date_'+str(blast_no)]-active_csn_df['date_of_last_trx']).dt.days
    
    # Step 8 - Create basket size segments
    active_csn_df['basket_size_l3m_'+str(blast_no)]=active_csn_df['basket_size_l3m']
    active_csn_df.drop(['basket_size_l3m'],axis=1,inplace=True)
    active_csn_df['basket_size_l3m_'+str(blast_no)]= pd.to_numeric(active_csn_df['basket_size_l3m_'+str(blast_no)],errors='coerce')
    
    active_csn_df.loc[(active_csn_df['basket_size_l3m_'+str(blast_no)]<30), 'Avg_bask_size_L3M_Segment_'+str(blast_no)]='Avg_bask_size_L3M_LT_30'     
    
    active_csn_df.loc[(active_csn_df['basket_size_l3m_'+str(blast_no)]>=30) & (active_csn_df['basket_size_l3m_'+str(blast_no)]<50),'Avg_bask_size_L3M_Segment_'+str(blast_no)]='Avg_bask_size_L3M_BTW_30_50'     
    active_csn_df.loc[(active_csn_df['basket_size_l3m_'+str(blast_no)]>=50),'Avg_bask_size_L3M_Segment_'+str(blast_no)]='Avg_bask_size_L3M_GT_50'     
    print("Sneak Peak :")
    print(active_csn_df.shape)     
    active_csn_df.to_csv(file_prefix+"_"+str(blast_no)+"_"+"Updated_Data"+"_"+str(lapser_month_year)+".csv",index=False)
    
def create_model_scoring_data(lapser_month_year,file_prefix,blast_no):
    from Model_Utils import check_missing_value

    # Read in updated base file
    base_df= pd.read_csv(file_prefix+"_"+str(blast_no)+"_"+"Updated_Data"+"_"+str(lapser_month_year)+".csv")
    cur.execute('select * from lac.FP_cloud_customer_360_new_'+str(lapser_month_year))       
    FP_df= pd.DataFrame(cur.fetchall())
    field_names = [k[0] for k in cur.description]
    FP_df.columns = field_names
    # drop old days_since_last_trx feature
    FP_df.drop(['days_since_last_trx','date_of_last_trx'],axis=1,inplace=True)    
    
    base_df['csn']=base_df['csn'].astype(int)
    FP_df['csn']=FP_df['csn'].astype(int)
    model_df = base_df.merge(FP_df,on='csn',how='left')
    model_df.head(5).to_csv("sample.csv")  
    print("Sneak Peak :")
    print(model_df.shape)  
    model_df.to_csv(file_prefix+"_"+str(blast_no)+"_Scoring_Data_Pre_"+str(lapser_month_year)+".csv",index=False)

    # check missing values
    missing_df = check_missing_value(model_df)
    missing_df.columns = ['feature','missing']
    missing_df.loc[:,'miss_rate']= (100*missing_df.loc[:,'missing'])/model_df.shape[0]
    missing_df.to_csv("Missing_Data_Report.csv",index=False)   
    
    # check datatypes
    type_df = pd.DataFrame(model_df.dtypes.reset_index())
    type_df.columns = ['feature','type']
    type_df.to_csv("Data_Type_Report.csv",index=False)    
    print("Scoring data preparation done")
    return 1

def process_model_scoring_data(file_prefix,blast_no,lapser_month_year):
    
    df=pd.read_csv(file_prefix+"_"+str(blast_no)+"_Scoring_Data_Pre_"+str(lapser_month_year)+".csv")
    
    col_toint=pd.read_csv("to_convert_to_integer.csv")
    col_tonumeric=pd.read_csv("to_convert_to_numeric.csv")
    col_tocat=pd.read_csv("to_convert_to_categ.csv")
    col_todate=pd.read_csv("to_convert_to_date.csv")
        
    df[col_tonumeric.feature.tolist()] = df[col_tonumeric.feature.tolist()].apply(pd.to_numeric,errors='coerce')
    df[col_tocat.feature.tolist()] = df[col_tocat.feature.tolist()].astype(object)
    df[col_todate.feature.tolist()] = df[col_todate.feature.tolist()].apply(pd.to_datetime,errors='coerce')
    df[col_toint.feature.tolist()] = df[col_toint.feature.tolist()].astype(int)
    
    # check datatypes
    type_df = pd.DataFrame(df.dtypes.reset_index())
    type_df.columns = ['feature','type']
    type_df.to_csv("Data_Type_Report_PostProcess.csv",index=False)    
        
  
    # drop any date column
    date_cols = df.select_dtypes(include=['datetime64','datetime']).columns
    df = df.drop(date_cols,axis=1)
        
    # write out the feature list
    feature_list=pd.DataFrame({'feature':df.columns.values})
    feature_list.to_csv("Feature_List_Post_Processing.csv")
           
    # replace missing with 0
    df = df.fillna(0)
    
    # create target variable    
    # 1 if CSN spend less than 50% of medium spend of last 3 months for next 1 month
    # 0 otherwise
    
    lapser_year = int(lapser_month_year/100)
    lapser_month = int(lapser_month_year%100)

    # Step 1: Check customers who were active M0,M1,M2
    if lapser_month == 1:
        month_L3 = [10,11,12]
        year_L3 = [lapser_year-1,lapser_year-1,lapser_year-1]
    elif lapser_month == 2:
        month_L3 = [11,12,1]
        year_L3 = [lapser_year-1,lapser_year-1,lapser_year] 
    elif lapser_month == 3:
        month_L3 = [12,1,2]
        year_L3 = [lapser_year-1,lapser_year,lapser_year]         
    elif lapser_month >= 4:
        month_L3 = [lapser_month-3,lapser_month-2,lapser_month-1]
        year_L3 = [lapser_year,lapser_year,lapser_year]
    
    Month_L3 =[]
    for i in month_L3:
        if i<10:
            ch = '0'+str(i)
        else:
            ch = str(i)
        Month_L3.append(ch)
    
    Year_L3=[]
    for i in year_L3:
        Year_L3.append(str(i))    

    df['spend_decrease_nextm']=0
    df.loc[df[['total_spend_'+Year_L3[0]+Month_L3[0],'total_spend_'+Year_L3[1]+Month_L3[1],'total_spend_'+Year_L3[2]+Month_L3[2]]].apply(np.median, axis=1)*0.5>=df['total_spend_nextm'],'spend_decrease_nextm']=1
    df.to_csv(file_prefix+"_"+str(blast_no)+"_Scoring_Data_PostProc"+str(lapser_month_year)+".csv",index=False)
   

    print("Sneak Peak :")
    print(df.shape)    
    return(1)

def create_model_scores(file_prefix,blast_no,lapser_month_year,model_name,method,model_prefix):
    
    from Model_Utils import create_lift_gain_chart    
    import pickle
    import pandas as pd
    import numpy as np
    
    df=pd.read_csv(file_prefix+"_"+str(blast_no)+"_Scoring_Data_PostProc"+str(lapser_month_year)+".csv")    
    feature_cols= df.columns.values[~np.isin(df.columns.values,['spend_decrease_nextm','csn','active_rate_l1m','total_spend_nextm','total_trx_nextm','total_points_issued_nextm','total_points_redeemed_nextm','lapser_date_1','Avg_bask_size_L3M_Segment_1','basket_size_l3m_'+str(blast_no),'store_code_of_acquisition','store_code_of_last_trx','store_code_of_2nd_last_trx','next_month','activity_status_detailed'])] 

    df_1 = df[feature_cols]

    #create dummy variables
    num_cols = df_1.select_dtypes(include=[int,float,'int32','int64','float32','float64']).columns
    cat_cols = df_1.select_dtypes(include=[object,'category']).columns   
    
    print(num_cols)
    print(cat_cols)
    
    # Drops categorical features that have more than 12 classes
    col_cat_drop = []
    col_cat_keep = []
    for col in cat_cols:
        if len(df_1[col].unique())>12:
            col_cat_drop.append(col)
            continue
        col_cat_keep.append(col)
    #print('col_cat_drop:\n',col_cat_drop)

    # Drops numerical features that have only 1 bin
    q = 5
    col_num_drop = []
    #col_num_bins = []
    col_num_keep = []
    for col in num_cols:
        col_num_keep.append(col)
    #print('col_num_drop:\n',col_num_drop)

    # unrolling of Categorical features
    temp_df = pd.get_dummies(df_1[col_cat_keep])
    col_cat_unroll = temp_df.columns.tolist()
    df_2 = temp_df.copy()

    # unrolling of Binned Numerical features
    #print('col_num_bins',col_num_bins)
    df_2[col_num_keep] = df_1[col_num_keep]
    #print('col_num_unroll:\n',col_num_unroll)

    # Consolidate unrolled features
    #col_fin = col_cat_unroll+col_num_unroll
    print('Final_Num_features:',df_2.shape[1])
    #print('Final_Num_features_unroll:',col_fin)
        
    df_2['spend_decrease_nextm']=df['spend_decrease_nextm']
    df_2['csn']=df['csn']
    
    feature_cols = df_2.columns.tolist()
    feature_list=pd.DataFrame({'feature':feature_cols})
    feature_list.to_csv(file_prefix+"_Feature_List_Post_Processing_Test.csv")
    
    feature_cols= pd.read_csv(model_prefix+'_Feature_List_Post_Processing_Train.csv').feature.values
           
    # Load libraries
    import pandas as pd
    from sklearn.tree import DecisionTreeClassifier # Import Decision Tree Classifier
    from sklearn.model_selection import train_test_split # Import train_test_split function
    from sklearn import metrics #Import scikit-learn metrics module for accuracy calculation
    import numpy as np
    from sklearn import tree
    from matplotlib import pyplot as plt

    feature_cols= feature_cols[~np.isin(feature_cols,['spend_decrease_nextm','csn'])] 
    X_test = df_2[feature_cols] # Features
    y_test = df_2.spend_decrease_nextm # Target variable

    # load the model from disk
    loaded_model = pickle.load(open(model_name, 'rb'))
        
    #Predict the response for test dataset
    y_pred = loaded_model.predict(X_test)    
        
    ###################################################################################
    #    Get Performance Metrics 
    ###################################################################################
    # Model Accuracy, how often is the classifier correct?
    print(" Test Accuracy:",metrics.accuracy_score(y_test, y_pred))
    import warnings
    from sklearn.metrics import confusion_matrix, accuracy_score
    warnings.filterwarnings('ignore')
    print("Confusion Matrix")
    print(confusion_matrix(y_test, y_pred))
    #Predict the response score for test dataset
    y_pred = loaded_model.predict_proba(X_test)
    
    import scikitplot as skplt
    skplt.metrics.plot_lift_curve(y_test, y_pred)
    plt.show()
    plt.savefig(file_prefix+"_"+method+"_lift_Curve"+str(lapser_month_year)+".png")

    skplt.metrics.plot_cumulative_gain(y_test,y_pred) 
    plt.show()
    plt.savefig(file_prefix+"_"+method+"_cumulative_gain"+str(lapser_month_year)+".png")
       

    # calculate AUC
    fpr, tpr, thresholds = metrics.roc_curve(y_test,  y_pred[:,1] , pos_label=1)                        
    print(" Test AUC :", round(metrics.auc(fpr, tpr),2))
    
                
    # Get Precision and Recall for test    
    y_pred=y_pred[:,1].tolist()
    y_pred_q = pd.qcut(y_pred,q=10,duplicates ='drop',labels=False)    
    y_test=y_test.tolist()    
    
    
    pred_df = pd.DataFrame({'y_actual':y_test,'y_pred':y_pred,'y_pred_quant':y_pred_q})
    pred_df['y_actual']= pd.to_numeric(pred_df['y_actual'],errors='coerce') 
    print(pred_df.head())
    performance_df=create_lift_gain_chart(df=pred_df,target='y_actual',prediction_quantile='y_pred_quant',prediction_score='y_pred')
    performance_df.to_csv(file_prefix+"_"+method+"_Performance_Test"+str(lapser_month_year)+".csv")
    
    ##############################################################################################
    
    # create scoring file
    df['time_gap_score_lapser_'+str(blast_no)]= y_pred 
    df['decile_time_gap_score_lapser_'+str(blast_no)]= pd.qcut(df['time_gap_score_lapser_'+str(blast_no)], 10, labels=False,duplicates='drop')    
    
    print("\n")
    print("Prediction score quantiles count :", df['decile_time_gap_score_lapser_'+str(blast_no)].nunique())
    print("Prediction score quantiles :", df['decile_time_gap_score_lapser_'+str(blast_no)].unique())
    
    for x in range(0,10):
        df['lapser_'+str(blast_no)+"_TopD"+str(10-x)]=0
        df.loc[df['decile_time_gap_score_lapser_'+str(blast_no)] >= x,'lapser_'+str(blast_no)+"_TopD"+str(10-x)]=1
       
    df['Base_'+str(blast_no)]= blast_no
    df['blast_no']= blast_no
    
    # write out base file - with all cutomers eligible for lapser engine  
    df.to_csv(file_prefix+"_"+method+"_"+str(blast_no)+"_lapser_Scored_List"+"_"+str(lapser_month_year)+".csv", index=False)
    print(df.columns)

def create_model_scores_new(file_prefix,blast_no,lapser_month_year,model_name,method,model_prefix):
    
       
    import pickle
    
    df=pd.read_csv(file_prefix+"_"+str(blast_no)+"_Scoring_Data_PostProc"+str(lapser_month_year)+".csv")    
    feature_cols= df.columns.values[~np.isin(df.columns.values,['spend_decrease_nextm','csn','active_rate_l1m','total_spend_nextm','total_trx_nextm','total_points_issued_nextm','total_points_redeemed_nextm','lapser_date_1','Avg_bask_size_L3M_Segment_1','basket_size_l3m_'+str(blast_no),'store_code_of_acquisition','store_code_of_last_trx','store_code_of_2nd_last_trx','next_month','activity_status_detailed'])] 

    df_1 = df[feature_cols]

    #create dummy variables
    num_cols = df_1.select_dtypes(include=[int,float,'int32','int64','float32','float64']).columns
    cat_cols = df_1.select_dtypes(include=[object,'category']).columns   
    
    print(num_cols)
    print(cat_cols)
    
    # Drops categorical features that have more than 12 classes
    col_cat_drop = []
    col_cat_keep = []
    for col in cat_cols:
        if len(df_1[col].unique())>12:
            col_cat_drop.append(col)
            continue
        col_cat_keep.append(col)
    #print('col_cat_drop:\n',col_cat_drop)

    # Drops numerical features that have only 1 bin
    q = 5
    col_num_drop = []
    #col_num_bins = []
    col_num_keep = []
    for col in num_cols:
        col_num_keep.append(col)
    #print('col_num_drop:\n',col_num_drop)

    # unrolling of Categorical features
    temp_df = pd.get_dummies(df_1[col_cat_keep])
    col_cat_unroll = temp_df.columns.tolist()
    df_2 = temp_df.copy()

    # unrolling of Binned Numerical features
    df_2[col_num_keep] = df_1[col_num_keep]
    #print('col_num_unroll:\n',col_num_unroll)

    # Consolidate unrolled features
    print('Final_Num_features:',df_2.shape[1])
    #print('Final_Num_features_unroll:',col_fin)
          
    feature_cols = df_2.columns.tolist()
    feature_list=pd.DataFrame({'feature':feature_cols})
    feature_list.to_csv(file_prefix+"_Feature_List_Post_Processing_Test.csv")
    
    feature_cols= pd.read_csv(model_prefix+'_Feature_List_Post_Processing_Train.csv').feature.values
           
    # Load libraries
    
    from sklearn.model_selection import train_test_split # Import train_test_split function
    from sklearn import metrics #Import scikit-learn metrics module for accuracy calculation
    from matplotlib import pyplot as plt

    feature_cols= feature_cols[~np.isin(feature_cols,['spend_decrease_nextm','csn'])] 
    X_test = df_2[feature_cols] # Features
    
    # load the model from disk
    loaded_model = pickle.load(open(model_name, 'rb'))
        
    #Predict the response for test dataset
    y_pred = loaded_model.predict_proba(X_test)   
    y_pred=y_pred[:,1].tolist()
        
    # create scoring file
    df['time_gap_score_lapser_'+str(blast_no)]= y_pred 
    df['decile_time_gap_score_lapser_'+str(blast_no)]= pd.qcut(df['time_gap_score_lapser_'+str(blast_no)], 10, labels=False,duplicates='drop')    
    
    print("\n")
    print("Prediction score quantiles count :", df['decile_time_gap_score_lapser_'+str(blast_no)].nunique())
    print("Prediction score quantiles :", df['decile_time_gap_score_lapser_'+str(blast_no)].unique())
    
    for x in range(0,10):
        df['lapser_'+str(blast_no)+"_TopD"+str(10-x)]=0
        df.loc[df['decile_time_gap_score_lapser_'+str(blast_no)] >= x,'lapser_'+str(blast_no)+"_TopD"+str(10-x)]=1
       
    df['Base_'+str(blast_no)]= blast_no
    df['blast_no']= blast_no
    
    # write out base file - with all cutomers eligible for lapser engine  
    df.to_csv(file_prefix+"_"+method+"_"+str(blast_no)+"_lapser_Scored_List"+"_"+str(lapser_month_year)+".csv", index=False)
    print("Sneak Peak :")
    print(df.shape)
    print(df.columns)

            
def create_model_train_data(lapser_month_year,file_prefix,blast_no):
    from Model_Utils import check_missing_value
    
    # Read in base file
    base_df= pd.read_csv(file_prefix+"_"+str(blast_no)+"_"+"Base_Customer_List"+"_"+str(lapser_month_year)+".csv")

    cur.execute('select * from lac.FP_cloud_customer_360_new_'+str(lapser_month_year))    

    FP_df= pd.DataFrame(cur.fetchall())
    field_names = [k[0] for k in cur.description]
    FP_df.columns = field_names
  
    base_df['csn']=base_df['csn'].astype(int)
    FP_df['csn']=FP_df['csn'].astype(int)
    model_df = base_df.merge(FP_df,on='csn',how='left')
    model_df.head(5).to_csv("sample.csv")    
    model_df.to_csv(file_prefix+"_"+str(blast_no)+"_Modeling_Data_"+str(lapser_month_year)+".csv",index=False)

    # check missing values
    missing_df = check_missing_value(model_df)
    missing_df.columns = ['feature','missing']
    missing_df.loc[:,'miss_rate']= (100*missing_df.loc[:,'missing'])/model_df.shape[0]
    missing_df.to_csv("Missing_Data_Report.csv",index=False)   
    
    # check datatypes
    type_df = pd.DataFrame(model_df.dtypes.reset_index())
    type_df.columns = ['feature','type']
    type_df.to_csv("Data_Type_Report.csv",index=False)    

    return 1

def process_model_train_data(file_prefix,blast_no,lapser_month_year):
    
    import numpy as np        
    df=pd.read_csv(file_prefix+"_"+str(blast_no)+"_Modeling_Data_"+str(lapser_month_year)+".csv")
    
    col_toint=pd.read_csv("to_convert_to_integer.csv")
    col_tonumeric=pd.read_csv("to_convert_to_numeric.csv")
    col_tocat=pd.read_csv("to_convert_to_categ.csv")
    col_todate=pd.read_csv("to_convert_to_date.csv")
        
    df[col_tonumeric.feature.tolist()] = df[col_tonumeric.feature.tolist()].apply(pd.to_numeric,errors='coerce')
    df[col_tocat.feature.tolist()] = df[col_tocat.feature.tolist()].astype(object)
    df[col_todate.feature.tolist()] = df[col_todate.feature.tolist()].apply(pd.to_datetime,errors='coerce')
    df[col_toint.feature.tolist()] = df[col_toint.feature.tolist()].astype(int)
    
    # check datatypes
    type_df = pd.DataFrame(df.dtypes.reset_index())
    type_df.columns = ['feature','type']
    type_df.to_csv("Data_Type_Report_PostProcess.csv",index=False)    
        
    # drop any date column
    date_cols = df.select_dtypes(include=['datetime64','datetime']).columns
    df = df.drop(date_cols,axis=1)
        
      
    # replace missing with 0
    df = df.fillna(0)
        
    # create target variable    
    # 1 if CSN spend less than 50% of medium spend of last 3 months for next 1 month
    # 0 otherwise
    
    lapser_year = int(lapser_month_year/100)
    lapser_month = int(lapser_month_year%100)

    # Step 1: Check customers who were active M0,M1,M2
    if lapser_month == 1:
        month_L3 = [10,11,12]
        year_L3 = [lapser_year-1,lapser_year-1,lapser_year-1]
    elif lapser_month == 2:
        month_L3 = [11,12,1]
        year_L3 = [lapser_year-1,lapser_year-1,lapser_year] 
    elif lapser_month == 3:
        month_L3 = [12,1,2]
        year_L3 = [lapser_year-1,lapser_year,lapser_year]         
    elif lapser_month >= 4:
        month_L3 = [lapser_month-3,lapser_month-2,lapser_month-1]
        year_L3 = [lapser_year,lapser_year,lapser_year]
    
    Month_L3 =[]
    for i in month_L3:
        if i<10:
            ch = '0'+str(i)
        else:
            ch = str(i)
        Month_L3.append(ch)
    
    Year_L3=[]
    for i in year_L3:
        Year_L3.append(str(i))    

    df['spend_decrease_nextm']=0
    df.loc[df[['total_spend_'+Year_L3[0]+Month_L3[0],'total_spend_'+Year_L3[1]+Month_L3[1],'total_spend_'+Year_L3[2]+Month_L3[2]]].apply(np.median, axis=1)*0.5>=df['total_spend_nextm'],'spend_decrease_nextm']=1
   
    output1=pd.DataFrame()
    output2=pd.DataFrame()

    df.to_csv(file_prefix+"_"+str(blast_no)+"_Modeling_Data_PostProc"+str(lapser_month_year)+".csv",index=False)
    
    return(1)



def train_xgboost(file_prefix,blast_no,lapser_month_year,method):
    
    from Model_Utils import create_lift_gain_chart
    import pickle
    
    df=pd.read_csv(file_prefix+"_"+str(blast_no)+"_Modeling_Data_PostProc"+str(lapser_month_year)+".csv")
    
    feature_cols= df.columns.values[~np.isin(df.columns.values,['spend_decrease_nextm','csn','active_rate_l1m','total_spend_nextm','total_trx_nextm','total_points_issued_nextm','total_points_redeemed_nextm','lapser_date_1','Avg_bask_size_L3M_Segment_1','store_code_of_acquisition','store_code_of_last_trx','store_code_of_2nd_last_trx','next_month','activity_status_detailed','business_unit_of_2nd_last_trx'])] 
    df_1 = df[feature_cols]
    feature_list=pd.DataFrame({'feature':feature_cols})
    feature_list.to_csv('final_feature_list.csv')

    #create dummy variables
    num_cols = df_1.select_dtypes(include=[int,float,'int32','int64','float32','float64']).columns
    cat_cols = df_1.select_dtypes(include=[object,'category']).columns   
    
    print(num_cols)
    print(cat_cols)
    
    # Drops categorical features that have more than 12 classes
    col_cat_drop = []
    col_cat_keep = []
    for col in cat_cols:
        if len(df_1[col].unique())>12:
            col_cat_drop.append(col)
            continue
        col_cat_keep.append(col)
    #print('col_cat_drop:\n',col_cat_drop)

    # Drops numerical features that have only 1 bin
    q = 5
    col_num_drop = []
    #col_num_bins = []
    col_num_keep= []
    for col in num_cols:
        #print(col)
        if len(df_1[col].value_counts())<=1:
            col_num_drop.append(col)
            continue
        filled_col = df_1[col].fillna(0)
        col_num_keep.append(col)
    #print('col_num_drop:\n',col_num_drop)

    # unrolling of Categorical features
    temp_df = pd.get_dummies(df_1[col_cat_keep])
    col_cat_unroll = temp_df.columns.tolist()
    df_2 = temp_df.copy()

    # unrolling of Binned Numerical features
      
    df_2[col_num_keep] = df_1[col_num_keep]
    #print('col_num_unroll:\n',col_num_unroll)

    # Consolidate unrolled features
    #col_fin = col_cat_unroll+col_num_unroll
    print('Final_Num_features:',df_2.shape[1])
    #print('Final_Num_features_unroll:',col_fin)
        
    df_2['spend_decrease_nextm']=df['spend_decrease_nextm']
    df_2['csn']=df['csn']
    
    feature_cols = df_2.columns.tolist()
    feature_list=pd.DataFrame({'feature':feature_cols})
    feature_list.to_csv(file_prefix+"_Feature_List_Post_Processing_Train.csv")

    #df_2.to_csv(file_prefix+"_"+str(blast_no)+"_Modeling_Data_PostProc_withdummy"+str(lapser_month_year)+".csv",index=False)
    
    # Load libraries
    from sklearn.model_selection import train_test_split # Import train_test_split function
    from sklearn import metrics #Import scikit-learn metrics module for accuracy calculation
    from matplotlib import pyplot as plt
    import xgboost as xgb
    from sklearn.model_selection import GridSearchCV

    feature_cols= df_2.columns.values[~np.isin(df_2.columns.values,['spend_decrease_nextm','csn'])] 
    X = df_2[feature_cols] # Features
    y = df_2.spend_decrease_nextm # Target variable

    #feature_cols = df_2.columns.tolist()
    feature_list=pd.DataFrame({'feature':feature_cols})
    feature_list.to_csv(file_prefix+"_Feature_List_Post_Processing_Train.csv")

    # Split dataset into training set and test set
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=1) # 70% training and 30% test

    print("\n")
    print('Train event rate :', np.sum(y_train)/np.size(y_train))
    print('Test event rate :', np.sum(y_test)/np.size(y_test))
    print("\n")
    
    # run grid search to decide the best parameters

    print("Starting grid search ")
    # Create the parameter grid: gbm_param_grid
    gbm_param_grid = {
        'colsample_bytree': [0.3, 0.5],
        'n_estimators': [800,1000],
        'max_depth': [3,5],
        'eta' : [0.01]
    }

    # Instantiate the regressor: gbm
    gbm = xgb.XGBClassifier(objective='binary:logistic',nthread=4)

    # Perform grid search: grid_mse
    grid_auc = GridSearchCV(estimator=gbm, param_grid=gbm_param_grid,
                            scoring='f1', cv=3, verbose=1)
    grid_auc.fit(X, y)

    # Print the best parameters and lowest RMSE
    print("Best parameters found: ", grid_auc.best_params_)
    #print("Best ROC-AUC found: ", np.sqrt(np.abs(grid_auc.best_score_)))
    print("Best F1 found: ", np.sqrt(np.abs(grid_auc.best_score_)))
        
    # Create the parameter dictionary: params
    #params = {"objective":"binary:logistic", "max_depth":3,"colsample_bytree":0.75,"eta":0.01}
    params = grid_auc.best_params_

    # Instantiate the XGBClassifier: xg_cl
    xg_cl = xgb.XGBClassifier(objective='binary:logistic', params=params,seed=123)

    # Fit the classifier to the training set
    xg_cl.fit(X_train,y_train)

    # feature importance
    print(xg_cl.feature_importances_)
    feat_imp = pd.DataFrame({"Feature":feature_cols,"Importance":xg_cl.feature_importances_})
    feat_imp.to_csv(file_prefix+"_Xgboost_Feature_Importance.csv")
    
    # plot
    plt.bar(range(len(xg_cl.feature_importances_)), xg_cl.feature_importances_)
    #pyplot.show()
    plt.savefig(file_prefix+"_Xgboost_feature_importance.png")
    
    #Predict the response for test dataset
    y_pred = xg_cl.predict(X_test)

    # Model Accuracy, how often is the classifier correct?
    print(" Test Accuracy:",metrics.accuracy_score(y_test, y_pred))

    #Predict the response for train dataset
    y_pred = xg_cl.predict(X_train)

    print(" Train Accuracy:",metrics.accuracy_score(y_train, y_pred))
    
    ###################################################################################
    #Get AUC
    ###################################################################################
    
    #Predict the response score for train dataset
    y_pred = xg_cl.predict_proba(X_train)

    
    # calculate AUC
    fpr, tpr, thresholds = metrics.roc_curve(y_train, y_pred[:,1] , pos_label=1)                        
    print(" Train AUC :", round(metrics.auc(fpr, tpr),2))

    #Predict the response score for test dataset
    y_pred = xg_cl.predict_proba(X_test)
    from Model_Utils import create_lift_gain_chart, roc_plot, pr_plot
    roc_plot(y_test,y_pred[:,1])
    pr_plot(y_test,y_pred[:,1])
    
    # calculate AUC
    fpr, tpr, thresholds = metrics.roc_curve(y_test,  y_pred[:,1] , pos_label=1)                        
    print(" Test AUC :", round(metrics.auc(fpr, tpr),2))
    
                    
    # Get Precision and Recall for test
    
    y_pred=y_pred[:,1].tolist()
    y_pred_q = pd.qcut(y_pred,q=10,duplicates ='drop',labels=False)    
    y_test=y_test.tolist() 
   
    filename = 'final_model_'+method+'_'+file_prefix+'.sav'
    pickle.dump(xg_cl, open(filename, 'wb'))

    pred_df = pd.DataFrame({'y_actual':y_test,'y_pred':y_pred,'y_pred_quant':y_pred_q})
    pred_df['y_actual']= pd.to_numeric(pred_df['y_actual'],errors='coerce') 
    print(pred_df.head())
    performance_df=create_lift_gain_chart(df=pred_df,target='y_actual',prediction_quantile='y_pred_quant',prediction_score='y_pred')
    performance_df.to_csv(file_prefix+"_Xgboost_Performance_Train"+str(lapser_month_year)+".csv")
        
    #filename = 'final_model_'+method+'_'+file_prefix+'.sav'
    #pickle.dump(xg_cl, open(filename, 'wb'))
  
    return(1)



