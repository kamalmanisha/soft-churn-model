
# coding: utf-8

file_prefix= "Blast1/1T_OG"
file_prefix_old=""
tmy = int(input("Please enter lapser month year(format-yyyymm): "))
tdl = int(input("Please enter lapser month year date(format-yyyymmdd): "))

lapser_month_year = tmy
# lapser dates
lapser_date_long = tdl


# fix decile cut offs
decile_cutoff=4
#blast no
blast_no=1

#method
method='xgboost_f1'

#model prefix
model_prefix= "FP_Churn_OG"

#model name
model_name = 'final_model_'+method+'_'+model_prefix+'.sav'

control_size = 0.05

from FP_lapser_Engine_Helper_Functions import run_select_base_OG
from FP_lapser_Engine_Helper_Functions import update_ltd
from FP_lapser_Engine_Helper_Functions import create_model_scoring_data
from FP_lapser_Engine_Helper_Functions import process_model_scoring_data
from FP_lapser_Engine_Helper_Functions import create_model_scores_new
import sys
import time
timestr = time.strftime("%Y%m%d-%H%M%S")
sys.stdout = open(file_prefix+timestr+".txt", "w")


print("############################################################################")
print("############################################################################")
print("################## Blast No. :",blast_no,"##################################")    
print("############################################################################")
print("############################################################################")


print("############################################################################")
print("lapser Month :",lapser_month_year)
print("Blast No. :",blast_no)    
print("Blast Date :",lapser_date_long)  
print("Score threshold cutoff :",decile_cutoff)    

print("############################################################################")
print("\n")
print("############################################################################")
print("Starting Base file creation")
print("############################################################################")
print("\n")

# create base data
run_select_base_OG(lapser_month_year,file_prefix,blast_no)

print("############################################################################")
print("Base file creation completed")
print("############################################################################")
print("\n")

print("############################################################################")
print("Updating last trx date creation ...")
print("############################################################################")
print("\n")

# update days since last trx - no elimination of customers
update_ltd(lapser_month_year,file_prefix,method,blast_no,lapser_date_long)

print("############################################################################")
print("Updating last trx date completed")
print("############################################################################")
print("\n")

print("############################################################################")
print("Creating model scoring data")
print("############################################################################")
print("\n")

# create model scoring data
create_model_scoring_data(lapser_month_year,file_prefix,blast_no)

print("############################################################################")
print("Model scoring data creation completed")
print("############################################################################")
print("\n")

print("############################################################################")
print("Starting Model scoring data pre processing ...")
print("############################################################################")
print("\n")

# pre process scoring data
process_model_scoring_data(file_prefix,blast_no,lapser_month_year)

print("############################################################################")
print("Model scoring data pre processing completed ...")
print("############################################################################")
print("\n")

print("############################################################################")
print("Starting Model scoring ...")
print("############################################################################")
print("\n")

# create model scores
create_model_scores_new(file_prefix,blast_no,lapser_month_year,model_name,method,model_prefix)

print("############################################################################")
print("Model scoring complete...")
print("############################################################################")
print("\n")

print("############################################################################")
print("Creating final lapser list ...")
print("############################################################################")
print("\n")






