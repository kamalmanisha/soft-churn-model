def check_missing_value(df):
    import pandas as pd
    df_missing = pd.DataFrame(df.isnull().sum().reset_index())
    #df_missing
    print(df_missing.head())
    return df_missing

def roc_plot(y_test,y_pred_proba):
    import matplotlib.pyplot as plt
    from sklearn.metrics import roc_curve, auc,precision_recall_curve, average_precision_score
    #Print Area Under Curve
    plt.figure()
    fpr, tpr, thresholds = roc_curve(y_test, y_pred_proba)
    roc_auc = auc(fpr, tpr)
    plt.title('Receiver Operating Characteristic (ROC)')
    plt.plot(fpr, tpr, 'b', label = 'AUC = %0.3f' %roc_auc)
    plt.legend(loc='lower right')
    plt.plot([0,1], [0,1], 'r--')
    plt.xlim([0.0,1.0])
    plt.ylim([0.0,1.0])
    plt.ylabel('True Positive Rate')
    plt.xlabel('False Positive Rate')
    plt.show()
    plt.savefig('ROC_Curve.png')
    
def pr_plot(y_test,y_pred_proba):
    import matplotlib.pyplot as plt
    from sklearn.metrics import roc_curve, auc,precision_recall_curve, average_precision_score
    #Print Area Under Curve
    plt.figure()
    precision,recall,thresholds=precision_recall_curve(y_test,y_pred_proba)
    average_precision = average_precision_score(y_test, y_pred_proba)
    plt.title('Precision_Recall Curve AP=%0.2f'%average_precision,verticalalignment='center')
    plt.step(recall, precision,where='post',alpha=0.2,color='r')
    plt.fill_between(recall,precision,step='post',alpha=0.2,color='b')
    plt.xlim([0.0,1.0])
    plt.ylim([0.0,1.0])
    plt.ylabel('Precision')
    plt.xlabel('Recall')
    #plt.plot(precision,recall)
    plt.show()
    plt.savefig('PR_Curve.png')

def create_lift_gain_chart(df,target,prediction_quantile,prediction_score):  
    from sklearn import metrics
    from sklearn.metrics import precision_recall_curve
    from sklearn.metrics import f1_score
    from sklearn.metrics import auc
    from sklearn.metrics import average_precision_score
    import pandas as pd
    import numpy as np
    from matplotlib import pyplot as plt
    import seaborn as sns
    sns.set_style("darkgrid")

    true = df[target].sum()
    false = df[target].count()-df[target].sum()
    df['tp'] = 0 
    
    max_q=df[prediction_quantile].max()
    step=int(100/max_q)
    stop = (100+step)
    print('quantiles:')
    print(sorted(df[prediction_quantile].unique()))                              
                              
    df_summary = pd.DataFrame(df.groupby([prediction_quantile]).agg({target:['count','sum']}).reset_index())
    df_summary.columns = ['prediction_quantile','Total','True_Pos']
    
    df_summary['Event_Rate'] = (100*df_summary['True_Pos'])/df_summary['Total']
    df_summary['False_Pos'] = df_summary['Total']-df_summary['True_Pos']
    
    df_summary['True_Pos_Rate'] = 100*(df_summary['True_Pos']/true)
    df_summary['False_Pos_Rate'] = 100*(df_summary['False_Pos']/false)
    df_summary.sort_values('prediction_quantile',ascending=False,axis=0,inplace=True)
    
    df_summary['Cum_False_Pos_Rate'] = df_summary['False_Pos_Rate'].cumsum()    
    df_summary['Cum_True_Pos_Rate'] = df_summary['True_Pos_Rate'].cumsum()    
    #df_summary['Organic_Rate'] = range(step,stop,step)
    #df_summary['Lift'] = df_summary['Cum_True_Pos_Rate']/ df_summary['Organic_Rate']
    df_summary['False_Disc_Rate'] =(100* df_summary['False_Pos'])/df_summary['Total'] 

    # calculate AUC
    fpr, tpr, thresholds = metrics.roc_curve(np.array(df[target]), np.array(df[prediction_score]) , pos_label=1)                        
    df_summary['auc'] =  round(metrics.auc(fpr, tpr),2)
    temp_df=pd.DataFrame({'tpr':tpr,'fpr':fpr})
    temp_df['tpr_rate']=(100*temp_df['tpr'])/true
    temp_df['fpr_rate']=(100*temp_df['fpr'])/false    
    sns.lineplot(x="fpr_rate", y="tpr_rate", data=temp_df)
    #plt.savefig('AUC_Plot.png')   # save the figure to file
    plt.close()      
    print("AUC :",round(metrics.auc(fpr, tpr),2))

    # calculate precision-recall curve
    precision, recall, thresholds = precision_recall_curve(np.array(df[target]), np.array(df[prediction_score]))  
    df_summary['pr_recall_auc']=round(auc(recall, precision),2)
    print(len(thresholds))
    print(len(recall))
       
    temp_df=pd.DataFrame({'precision':precision,'recall':recall})
    sns.lineplot(x="precision", y="recall", data=temp_df)
    #plt.savefig('Precision_Recall_Plot.png')   # save the figure to file
    plt.close()      
    #plt.show()
    
    #output
    print("Performance Summary:")
    print("Total Actual Positives :",true)
    print("Total Actual Negatives :",false)
    print("Precision Recall AUC :", round(auc(recall, precision),2))    
    return df_summary

